import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Menu, MenuItem, Button } from '@material-ui/core';
import { ArrowDropDown } from '@material-ui/icons';

const replaceLang = (pathname, lang) => {
	let arr = pathname.split('/');
	arr[1] = lang;
	return arr.join('/');
}

const ChangeLanguage = (props) => {
	const { handleClose, handleClick, lang, value, location: { pathname } } = props;

	return (
		<div className="change-lang">
			<Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
				{value} <ArrowDropDown />
			</Button>
			<Menu id="simple-menu" anchorEl={lang} keepMounted open={Boolean(lang)} onClose={handleClose}>
				<Link to={replaceLang(pathname, 'en')}>
					<MenuItem onClick={handleClose}>en</MenuItem>
				</Link>
				<Link to={replaceLang(pathname, 'ar')}>
					<MenuItem onClick={handleClose}>ar</MenuItem>
				</Link>
				<Link to={replaceLang(pathname, 'es')}>
					<MenuItem onClick={handleClose}>es</MenuItem>
				</Link>
			</Menu>
		</div>
	);
};

export default withRouter(ChangeLanguage);
