import React from 'react';
import { Link } from 'react-router-dom';
import { BusinessDropdown } from 'business-component';

const PUBLIC_URL = process.env.PUBLIC_URL;

const TopNav = (props) => (
    <div className="topbar">
        <div className="topbar-left">
            <Link href="../analytics/analytics-index.html" className="logo">
                <span>
                    <img src={`${PUBLIC_URL}/assets/images/logo-sm.png`} alt="logo-small" className="logo-sm" />
                </span>
                <span>
                    <img src={`${PUBLIC_URL}/assets/images/logo-dark.png`} alt="logo-large" className="logo-lg" />
                </span>
                <span>
                    <img src={`${PUBLIC_URL}/assets/images/logo.png`} alt="logo-large" className="logo-light" />
                </span>
            </Link>
        </div>
        <nav className="navbar-custom">
            <ul className="list-unstyled topbar-nav float-right mb-0">
                <li className="hidden-sm">
                    <Link
                        to="#"
                        className="nav-link dropdown-toggle waves-effect waves-light"
                        data-toggle="dropdown"
                        role="button"
                        aria-haspopup="false" aria-expanded="false">
                        English <img src="../assets/images/flags/us_flag.jpg" className="ml-2" height="16" alt="" /> <i className="mdi mdi-chevron-down"></i>
                    </Link>
                    <div className="dropdown-menu dropdown-menu-right">
                        <Link className="dropdown-item" to="#"><span> German </span><img src="../assets/images/flags/germany_flag.jpg" alt="" className="ml-2 float-right" height="14" /></Link>
                        <Link className="dropdown-item" to="#"><span> Italian </span><img src="../assets/images/flags/italy_flag.jpg" alt="" className="ml-2 float-right" height="14" /></Link>
                        <Link className="dropdown-item" to="#"><span> French </span><img src="../assets/images/flags/french_flag.jpg" alt="" className="ml-2 float-right" height="14" /></Link>
                        <Link className="dropdown-item" to="#"><span> Spanish </span><img src="../assets/images/flags/spain_flag.jpg" alt="" className="ml-2 float-right" height="14" /></Link>
                        <Link className="dropdown-item" to="#"><span> Russian </span><img src="../assets/images/flags/russia_flag.jpg" alt="" className="ml-2 float-right" height="14" /></Link>
                    </div>
                </li>


                {/* <li className="dropdown notification-list">
                    <Link className="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="false" aria-expanded="false">
                        <i className="dripicons-bell noti-icon"></i>
                        <span className="badge badge-danger badge-pill noti-icon-badge">2</span>
                    </Link>
                    <div className="dropdown-menu dropdown-menu-right dropdown-lg">

                        <h6 className="dropdown-item-text">
                            Notifications (18)
                            </h6>
                        <div className="slimscroll notification-list">

                            <Link href="javascript:void(0);" className="dropdown-item notify-item active">
                                <div className="notify-icon bg-success"><i className="mdi mdi-cart-outline"></i></div>
                                <p className="notify-details">Your order is placed<small className="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                            </Link>

                            <Link href="javascript:void(0);" className="dropdown-item notify-item">
                                <div className="notify-icon bg-warning"><i className="mdi mdi-message"></i></div>
                                <p className="notify-details">New Message received<small className="text-muted">You have 87 unread messages</small></p>
                            </Link>

                            <Link href="javascript:void(0);" className="dropdown-item notify-item">
                                <div className="notify-icon bg-info"><i className="mdi mdi-glass-cocktail"></i></div>
                                <p className="notify-details">Your item is shipped<small className="text-muted">It is a long established fact that a reader will</small></p>
                            </Link>

                            <Link href="javascript:void(0);" className="dropdown-item notify-item">
                                <div className="notify-icon bg-primary"><i className="mdi mdi-cart-outline"></i></div>
                                <p className="notify-details">Your order is placed<small className="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                            </Link>

                            <Link href="javascript:void(0);" className="dropdown-item notify-item">
                                <div className="notify-icon bg-danger"><i className="mdi mdi-message"></i></div>
                                <p className="notify-details">New Message received<small className="text-muted">You have 87 unread messages</small></p>
                            </Link>
                        </div>

                        <Link href="javascript:void(0);" className="dropdown-item text-center text-primary">
                            View all <i className="fi-arrow-right"></i>
                        </Link>
                    </div>
                </li> */}

                <li className="dropdown">
                    <Link className="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="false" aria-expanded="false">
                        <img
                            src={`https://instagram.fslv1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/76893584_2454370434776617_8581488023416340480_n.jpg?_nc_ht=instagram.fslv1-2.fna.fbcdn.net&_nc_ohc=_RcJYnZljK4AX9v-GpY&oh=02a3dd9643a41d5ab067e4be6c8a40e1&oe=5EEBDDFD`}
                            alt="profile-user" className="rounded-circle" />
                        <span className="ml-1 nav-user-name hidden-sm">Swapnil <i className="mdi mdi-chevron-down"></i> </span>
                    </Link>
                    <div className="dropdown-menu dropdown-menu-right">
                        <Link className="dropdown-item" href="#"><i className="dripicons-user text-muted mr-2"></i> Profile</Link>
                        <Link className="dropdown-item" href="#"><i className="dripicons-wallet text-muted mr-2"></i> My Wallet</Link>
                        <Link className="dropdown-item" href="#"><i className="dripicons-gear text-muted mr-2"></i> Settings</Link>
                        <Link className="dropdown-item" href="#"><i className="dripicons-lock text-muted mr-2"></i> Lock screen</Link>
                        <div className="dropdown-divider"></div>
                        <Link className="dropdown-item" href="#" onClick={() => {
                            localStorage.setItem('token', '');
                            window.location.replace('/en/login');
                        }}>
                            <i className="dripicons-exit text-muted mr-2"></i> Logout
                        </Link>
                    </div>
                </li>
            </ul>
            <ul className="list-unstyled topbar-nav mb-0">
                <li>
                    <button className="button-menu-mobile nav-link waves-effect waves-light">
                        <i className="dripicons-menu nav-icon"></i>
                    </button>
                </li>
                {/* <li className="hide-phone app-search">
                    <form role="search" className="">
                        <input type="text" placeholder="Search..." className="form-control" />
                        <Link href=""><i className="fas fa-search"></i></Link>
                    </form>
                </li> */}
                <BusinessDropdown businessId={props.match.params.businessId} />
            </ul>
        </nav>
    </div>
);

export default TopNav;