import React, { Component } from 'react';
import { Route, Redirect, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Dashboard from '../Dashboard';
import Itinerary from '../Itinerary';
import Proposal from '../Proposal';
import Builder from '../Itinerary/Builder';
import Calendar from '../Calendar';
import { Contacts } from 'contacts-component';
import TopNav from './TopNav';
import LeftNav from '../LeftNav';
import Login from '../Auth/Login';
import SignUp from '../Auth/SignUp';
import Pricing from '../Pricing';
import ItineraryBasic from '../Itinerary-Basic/components';
import ForgotPassword from '../Auth/ForgotPassword';
import ResetPassword from '../Auth/ResetPassword';
import { businessActions, Business } from 'business-component';
import Loader from '../Loader';

class Layout extends Component {
	state = {
		isLoggedIn: false,
	}

	isTokenValid = () => {
		const token = this.getToken();

		if (token) {
			this.setState({ isLoggedIn: true });
			this.getBusinesses();
		}
	}

	getBusinesses = async () => {
		console.log('test-------------test');
		const { dispatch, businesses } = this.props;
		dispatch(businessActions.handleFetchBusinesses());
	}

	getToken = () => {
		return localStorage.getItem('token');
	}

	componentDidMount() {
		this.isTokenValid();
	}

	render() {
		const { isLoggedIn, totalBusiness } = this.state;
		const { businesses, match } = this.props;

		if (!isLoggedIn) {
			return (
				<Switch>
					<Route path="/:lang/login" component={Login} />
					<Route path="/:lang/register" component={SignUp} />
					<Route path="/:lang/pricing" component={Pricing} />
					<Route path="/:lang/forgot-password" component={ForgotPassword} />
					<Route path="/:lang/reset-password" component={ResetPassword} />
					<Redirect to="/en/login" />
				</Switch>
			);
		}

		if (businesses.isFetching) {
			return <Loader />
		}

		const business = businesses.data[Object.keys(businesses.data || {})[0]] || {};

		if (!business.id) {
			return (
				<Switch>
					<Route path="/en/business" component={Business} />
					<Redirect to="/en/business/add" />
				</Switch>
			);
		}

		return (
			<div className="dark-topbar">
				<Route path="/:lang/:businessId" component={TopNav} />
				{/* <TopNav businessId={match.params.businessId} /> */}
				<div className="page-wrapper">
					<Route path="/:lang/:businessId" component={LeftNav} />
					<Switch>
						<Route path="/:lang/:businessId/analytics/dashboard" component={Dashboard} />
						<Route path="/:lang/:businessId/itineraries" component={Itinerary} />
						<Route path="/:lang/:businessId/proposals" component={Proposal} />
						<Route path="/:lang/:businessId/itinerary/builder/:itineraryId" component={Builder} />
						<Route path="/:lang/:businessId/calendar" component={Calendar} />
						<Route path="/:lang/:businessId/applications/itinerary" component={ItineraryBasic} />
						<Route path="/:lang/:businessId/applications/contacts" component={Contacts} />
						<Redirect to={`/en/${business.id}/analytics/dashboard`} />
					</Switch>
				</div>
				<Route path="/:lang/pricing" component={Pricing} />
			</div>
		);
	}
}

const mapStateToProps = ({ businesses }, props) => {
	return {
		businesses: businesses
	};
}

export default withRouter(connect(mapStateToProps)(Layout));
