import React from 'react';

const styles ={
	loader: {
		width: '100%',
		textAlign: 'center',
		marginTop: '10%'
	}
}

const Loader = () => (
	<div style={styles.loader}>
		<div class="spinner-border text-primary" role="status">
		</div>
	</div>
);

export default Loader;