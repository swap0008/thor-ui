import React, { Component } from 'react';
import { connect } from 'react-redux';
import serializeForm from 'form-serialize';
import { handleSaveBusiness } from '../../redux/actions/business';

class Business extends Component {
    handleFormSubmit = (e) => {
        e.preventDefault();
        const data = serializeForm(e.target, { hash: true });

        this.props.dispatch(handleSaveBusiness(data));
    }

    render() {
        return (
            <div className="row" style={{
                marginTop: '5%',
                marginLeft: '15%'
            }}>
                <div className="col-lg-10">
                    <div className="card">
                        <div className="card-body">
                            <h4 className="mt-0 header-title">Business Details</h4>
                            <p className="text-muted mb-3">Basic example to demonstrate Bootstrap’s form styles.</p>
                            <form onSubmit={this.handleFormSubmit} method="post">
                                <div className="form-group">
                                    <label>Business Name</label>
                                    <input type="text" name="name" className="form-control" placeholder="Enter name" />
                                </div>
                                <div className="form-group">
                                    <label>Country</label>
                                    <input type="text" name="country" className="form-control" placeholder="Country" />
                                </div>
                                <div className="form-group">
                                    <label>State</label>
                                    <input type="text" name="state" className="form-control" placeholder="Enter state" />
                                </div>
                                <div className="form-group">
                                    <label>City</label>
                                    <input type="text" name="city" className="form-control" placeholder="Enter city" />
                                </div>
                                <div className="form-group">
                                    <label>Pincode</label>
                                    <input type="text" name="pincode" className="form-control" placeholder="Enter pincode" />
                                </div>
                                <div className="form-group form-check">
                                    <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                    <label className="form-check-label" for="exampleCheck1">Check me out</label>
                                </div>
                                <button type="submit" className="btn btn-gradient-primary">Submit</button>
                                <button type="button" className="btn btn-gradient-danger">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect()(Business);