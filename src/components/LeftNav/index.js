import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { handleFetchLeftNavItems } from '../../redux/actions/leftNav';

class LeftNav extends Component {
    componentDidMount() {
        this.props.dispatch(handleFetchLeftNavItems());
    }

    darkSideNav = () => {
        window.$('body').toggleClass('dark-sidenav');
    }

    render() {
        const { match: { params: { lang, businessId } }, location: { pathname, hash }, leftNavItems } = this.props;

        return (
            <div className="left-sidenav mm-active">
                <ul className="metismenu left-sidenav-menu mm-show">

                    {leftNavItems.map(menu => {
                        const activeMenu = hash.includes(`#${menu.link}`) ? 'mm-active' : '';

                        return menu.items.length && (
                            <li
                                className={activeMenu}
                                key={menu.name}
                            >
                                <a className="active" href={`#${menu.link}`}>
                                    <i className={menu.logo}></i>
                                    <span>{menu.name}</span>
                                    <span className="menu-arrow"><i className="mdi mdi-chevron-right"></i></span>
                                </a>
                                <ul
                                    className={`nav-second-level ${activeMenu ? '' : 'collapse'}`}
                                    aria-expanded="false"
                                >
                                    {menu.items.map(item => {
                                        const activeSubMenu = pathname.includes(`/${menu.link}/${item.link}`) ? 'active' : '';

                                        return (
                                            <li className={`nav-item ${activeSubMenu}`} key={item.name}>
                                                <Link
                                                    className={`nav-link ${activeSubMenu}`}
                                                    to={`/${lang}/${businessId}/${menu.link}/${item.link}#${menu.link}`}
                                                >
                                                    <i className="ti-control-record"></i>{item.name}
                                                </Link>
                                            </li>
                                        );
                                    })}
                                </ul>
                            </li>
                        );
                    })}

                    <li>
                        <Link to={`#${hash}`} onClick={this.darkSideNav}>
                            <i className="ti-palette"></i>
                            <span>Dark/Light Sidenav</span>
                        </Link>
                    </li>
                </ul>
            </div>
        );
    }
}

const mapStateToProps = ({ leftNavItems }) => {
    return { leftNavItems };
}

export default withRouter(connect(mapStateToProps)(LeftNav));