import { defineMessages } from 'react-intl';

const messages = [
	{
		id: 'leftnav.dashboard',
		defaultMessage: 'Dashboard',
		link: 'dashboard',
		position: 0
	},
	{
		id: 'leftnav.itineraries',
		defaultMessage: 'Itineraries',
		link: 'itineraries',
		position: 1
	},
	{
		id: 'leftnav.proposal',
		defaultMessage: 'Proposals',
		link: 'proposals',
		position: 2
	},
	{
		id: 'leftnav.calendar',
		defaultMessage: 'Calendar',
		link: 'calendar',
		position: 3
	},
	{
		id: 'leftnav.leads',
		defaultMessage: 'Leads',
		link: 'leads',
		position: 4,
		subMenu: [
			{
				id: 'leftnav.leads.appointments',
				defaultMessage: 'Appointments',
				link: 'leads/appointments',
			},
			{
				id: 'leftnav.leads.campaigns',
				defaultMessage: 'Campaigns',
				link: 'leads/campaigns',
			},
			{
				id: 'leftnav.leads.builder',
				defaultMessage: 'From Builder',
				link: 'leads/from-builder',
			}
		]
	},
	{
		id: 'leftnav.templates',
		defaultMessage: 'Templates',
		link: 'templates',
		position: 5
	},
	{
		id: 'leftnav.contacts',
		defaultMessage: 'Contacts',
		link: 'contacts',
		position: 6
	},
	{
		id: 'leftnav.settings',
		defaultMessage: 'Settings',
		link: 'settings',
		position: 7
	}
];

export default defineMessages(messages);
