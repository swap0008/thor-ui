import React, { useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { List, ListItem, ListItemText, Divider, Collapse } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { injectIntl } from 'react-intl';
import messages from './data';

const useStyles = makeStyles((theme) => ({
	nested: {
		paddingLeft: theme.spacing(4)
	},
	listItemText: {
		fontSize: '16.3px',
		fontFamily: `'Lato', sans-serif`,
		fontStyle: 'italic'
	},
	listItemTextSelected: {
		fontSize: '16px',
		fontFamily: `'Lato', sans-serif`,
		fontStyle: 'italic',
		fontWeight: 600
	},
	leftNavList: {
		paddingTop: theme.spacing(0)
	}
}));

const LeftNav = (props) => {
	const classes = useStyles();
	const { intl: { formatMessage }, match: { params: { lang } }, location: { pathname } } = props;
	const [ selected, setSelected ] = useState(pathname.split('/')[2]);

	return (
		<div className="left-nav">
			<List className={classes.leftNavList}>
				{messages.map((msg) => (
					<React.Fragment key={msg.id}>
						<Link to={`/${lang}/${msg.link}`} onClick={() => setSelected(msg.link)}>
							<ListItem button className={selected === msg.link ? 'selected' : ''}>
								<ListItemText
									primary={formatMessage(msg)}
									classes={{
										primary:
											selected === msg.link ? classes.listItemTextSelected : classes.listItemText
									}}
								/>
							</ListItem>
						</Link>
						<Divider />
						{msg.subMenu && (
							<React.Fragment>
								<Collapse in={true} timeout="auto" unmountOnExit>
									<List component="div" disablePadding>
										{msg.subMenu.map((subMsg) => (
											<Link
												to={`/${lang}/${subMsg.link}`}
												key={subMsg.id}
												onClick={() => setSelected(msg.link)}
											>
												<ListItem button>
													<ListItemText
														primary={formatMessage(subMsg)}
														className={classes.nested}
														classes={{ primary: classes.listItemText }}
													/>
												</ListItem>
											</Link>
										))}
									</List>
								</Collapse>
								<Divider />
							</React.Fragment>
						)}
					</React.Fragment>
				))}
			</List>
		</div>
	);
};

export default withRouter(injectIntl(LeftNav));
