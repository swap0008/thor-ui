import React, { Component } from 'react';
import readXlsxFile from 'read-excel-file';
import serializeForm from 'form-serialize';
import Axios from 'axios';

class Dashboard extends Component {
	state = {
		selectMapping: false,
		showMapping: false,
		rows: [],
		mappedData: {}
	}

	handleReadFile = (e) => {
		const file = Array.from(e.target.files)[0];

		readXlsxFile(file).then(rows => {
			console.log(rows);
			this.setState({ rows, selectMapping: true });
		});
	}

	handleImport = () => {
		const { rows, mappedData } = this.state;
		const mappedKeys = Object.values(mappedData);

		const contacts = [];

		for (const row of rows) {
			const contact = {};
			row.forEach((r, i) => {
				if (mappedKeys[i] !== 'none') {
					contact[mappedKeys[i]] = r;
				}
			});
			contacts.push(contact);
		}

		Axios.post('http://localhost:4003/import/file/HmicA2mO5CNJs3waerp9', { contacts }).then(res => console.log(res));
	}


	handleMapping = (e) => {
		e.preventDefault();

		const data = serializeForm(e.target, { hash: true });

		const values = Object.values(data).slice().sort();
		for (let i = 0; i < values.length; i++) {
			if (values[i + 1] === values[i] && values[i] !== 'none')
				return alert(`${values[i]} already mapped.`);
		}

		this.setState({ selectMapping: false, showMapping: true, mappedData: data });
	}

	render() {
		const { selectMapping, rows, showMapping, mappedData } = this.state;

		if (selectMapping) {
			return (
				<div id="overlay" style={{ display: 'flex' }}>
					<div className="mapping-box">
						<h3>Headers Mapping</h3>
						<div className="mapping-data">
							<span><b>Headers</b></span>
							<span><b>Map To</b></span>
						</div>
						<form onSubmit={this.handleMapping}>
							{rows[0].map(header => (
								<div className="mapping-data" key={header}>
									<span>{header}</span>
									<select className="form-control" name={header} defaultValue={mappedData[header] ? mappedData[header] : ''}>
										<option>none</option>
										<option>first_name</option>
										<option>last_name</option>
										<option>phone</option>
										<option>address</option>
										<option>email_address</option>
										<option>country</option>
									</select>
								</div>
							))}
							<button type="submit" className="btn btn-gradient-primary btn-margin-right">
								Map
							</button>
							<button className="btn btn-gradient-danger"
								onClick={() => this.setState({ selectMapping: false, showMapping: false })}>
								Cancel
							</button>
						</form>
					</div>
				</div >
			);
		}

		if (showMapping) {
			return (
				<div id="overlay" style={{ display: 'flex' }}>
					<div className="mapping-box">
						<h3>Headers Mapping</h3>
						<div className="mapping-data">
							<span><b>Headers</b></span>
							<span><b>Mapped To</b></span>
						</div>
						{Object.keys(mappedData).map(key => (
							<div className="mapping-data" key={key}>
								<span>{key}</span>
								<select className="form-control" disabled={true}>
									<option>{mappedData[key]}</option>
								</select>
							</div>
						))}
						<button className="btn btn-gradient-primary btn-margin-right" onClick={this.handleImport}>
							Import
						</button>
						<button className="btn btn-gradient-danger"
							onClick={() => this.setState({ selectMapping: true, showMapping: false })}>
							Back
						</button>
					</div>
				</div >
			);
		}

		return (
			<div className="dashboard">
				<h1>Dashboard</h1>
				<input type="file" onChange={this.handleReadFile} />
			</div>
		);
	}
}

export default Dashboard;
