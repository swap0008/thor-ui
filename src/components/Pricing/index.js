import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Axios from 'axios';

class Pricing extends Component {
    state = {

    }

    submitPlanType = async (plan_type) => {
        const { match: { params: { lang } } } = this.props;

        const { data } = await Axios({
            method: 'post',
            url: 'http://localhost:3000/assign-role',
            data: { plan_type }
        });

        if (data) window.location.replace(`/${lang}/dashboard`);
    }

    render() {
        return (
            <div className="page-wrapper">
                <div className="page-content">

                    <div className="container-fluid">

                        <div className="row">
                            <div className="col-sm-12">
                                <div className="page-title-box">
                                    <div className="float-right">
                                        <ol className="breadcrumb">
                                            <li className="breadcrumb-item"><a href="javascript:void(0);">Login/SignUp</a></li>
                                            <li className="breadcrumb-item"><a href="javascript:void(0);">Select Plan</a></li>
                                            <li className="breadcrumb-item active">Dashboard</li>
                                        </ol>
                                    </div>
                                    <h4 className="page-title">Pricing</h4>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-3">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="pricingTable1">
                                            <span className="pricing-icon"><i className="fas fa-user"></i></span>
                                            <h6 className="title1 py-3 mt-2 mb-0">Basic plan <small className="text-muted">Per Month</small></h6>
                                            <ul className="list-unstyled pricing-content-2 pb-3">
                                                <li>300 Contacts Limit</li>
                                                <li>Link upto 2 Google Accounts</li>
                                                <li>Google Calendars, Contacts & much more</li>
                                                <li>Free BMW</li>
                                                <li>Europe Trip</li>
                                            </ul>
                                            <div className="text-center">
                                                <h3 className="amount">$39.00<small className="font-12 text-muted">/month</small></h3>
                                            </div>
                                            <span className="pricingTable-signup mt-3" onClick={() => this.submitPlanType('BASIC')}>Select</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="pricingTable1">
                                            <span className="badge badge-warning float-right a-animate-blink">POPULAR</span>
                                            <span className="pricing-icon"><i className="fas fa-rocket"></i></span>
                                            <h6 className="title1 py-3 mt-2 mb-0">Premium plan <small className="text-muted">Per Month</small></h6>
                                            <ul className="list-unstyled pricing-content-2 pb-3">
                                                <li>50GB Disk Space</li>
                                                <li>50 Email Accounts</li>
                                                <li>50GB Monthly Bandwidth</li>
                                                <li>10 Subdomains</li>
                                                <li>15 Domains</li>
                                            </ul>
                                            <div className="text-center">
                                                <h3 className="amount">$49.00<small className="font-12 text-muted">month</small></h3>
                                            </div>
                                            <span className="pricingTable-signup mt-3" onClick={() => this.submitPlanType('BASIC')}>Select</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="pricingTable1">
                                            <span className="pricing-icon"><i className="fas fa-envelope"></i></span>
                                            <h6 className="title1 py-3 mt-2 mb-0">Plus plan <small className="text-muted">Per Month</small></h6>
                                            <ul className="list-unstyled pricing-content-2 pb-3">
                                                <li>80GB Disk Space</li>
                                                <li>80 Email Accounts</li>
                                                <li>80GB Monthly Bandwidth</li>
                                                <li>15 Subdomains</li>
                                                <li>20 Domains</li>
                                            </ul>
                                            <div className="text-center">
                                                <h3 className="amount">$69.00<small className="font-12 text-muted">/month</small></h3>
                                            </div>
                                            <span href="#" className="pricingTable-signup mt-3">Select</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="pricingTable1">
                                            <span className="pricing-icon"><i className="fas fa-box"></i></span>
                                            <h6 className="title1 py-3 mt-2 mb-0">Master plan <small className="text-muted">Per year</small></h6>
                                            <ul className="list-unstyled pricing-content-2 pb-3">
                                                <li>180GB Disk Space</li>
                                                <li>180 Email Accounts</li>
                                                <li>180GB Yearly Bandwidth</li>
                                                <li>50 Subdomains</li>
                                                <li>40 Domains</li>
                                            </ul>
                                            <div className="text-center">
                                                <h3 className="amount">$199.00<small className="font-12 text-muted">/One year</small></h3>
                                            </div>
                                            <span href="#" className="pricingTable-signup mt-3">Select</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <footer className="footer text-center text-sm-left">
                        &copy; 2019 Envision Labs <span className="text-muted d-none d-sm-inline-block float-right">Crafted with <i className="mdi mdi-heart text-danger"></i></span>
                    </footer>
                </div>
            </div>
        );
    }
}

export default withRouter(Pricing);