import { defineMessages } from 'react-intl';

export const tripCardDetails = defineMessages([
    {
		id: 'tripcardinfo.daysAnd',
		defaultMessage: 'Days and'
	},
	{
		id: 'tripcardinfo.nights',
		defaultMessage: 'Nights'
	},
	{
		id: 'tripcardinfo.estimatedCost',
		defaultMessage: 'Estimated Cost'
	},
	{
		id: 'tripcardinfo.totalProposalsMade',
		defaultMessage: 'Total Proposals Made'
	},
	{
		id: 'tripcardinfo.activeProposals',
		defaultMessage: 'Active Proposals'
	},
	{
		id: 'tripcardinfo.proposal',
		defaultMessage: 'Proposal'
	},
	{
		id: 'tripcardinfo.customerName',
		defaultMessage: 'Customer Name'
	},
	{
		id: 'tripcardinfo.proposalName',
		defaultMessage: 'Proposal Name'
	},
	{
		id: 'tripcardinfo.cost',
		defaultMessage: 'Cost'
	}
]);