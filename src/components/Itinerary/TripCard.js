import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { HotelTwoTone, TrainTwoTone, DirectionsBoatTwoTone, FlightTwoTone } from '@material-ui/icons';

class TripCard extends Component {
	render() {
		const { card, match: { params: { lang } }, history: { push } } = this.props;

		return (
			<div className="card-wrapper" key={card.key}>
				<Link to={`/${lang}/itineraries/${card.id}`}>
					<div className="location-image" style={{
						backgroundImage: `url(${card.cover_img})`
					}}>
						<div className="image-layer" />
						<span className="image-center">{card.name}</span>
					</div>
				</Link>
				<span className="type">Itinerary</span>
				<h3>{card.name}</h3>
				<div className="icons-days">
					<div className="icons">
						<HotelTwoTone fontSize="small" />
						<TrainTwoTone fontSize="small" />
						<DirectionsBoatTwoTone fontSize="small" />
						<FlightTwoTone fontSize="small" />
					</div>
					<div className="days">
						11D · 10N
                    </div>
				</div>
				<div className="price-edit">
					<span className="price">₹ 1,20,000</span>
					<button className="edit" onClick={() => push(`/${lang}/itinerary/builder/${card.id}`)}>
						Edit
                    </button>
				</div>
			</div>
		);
	}
}

export default withRouter(TripCard);