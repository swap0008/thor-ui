import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Fab } from '@material-ui/core';
import { Search, Add } from '@material-ui/icons';
import TripCard from './TripCard';
import LeftNav from '../LeftNav';
import RightNav from '../RightNav';

class Itinerary extends Component {
	render() {
		const { itineraries = {}, match: { params: { lang } } } = this.props;

		return (
			<div className="sections">
				<LeftNav />
				<div className="itinerary">
					<div className="search">
						<input type="text" className="search-box" placeholder="Search" />
						<Search className="search-icon" />
					</div>
					<div className="filter" />
					<div className="trip">
						{Object.keys(itineraries).map((key) => <TripCard key={key} card={itineraries[key]} />)}
					</div>

					<Link to={`/${lang}/itinerary/builder`}>
						<Fab color="primary" aria-label="add" size="medium">
							<Add />
						</Fab>
					</Link>
				</div>
				<RightNav />
			</div>
		);
	}
}

const mapStateToProps = ({ itineraries }) => {
	return {
		itineraries
	};
}

export default withRouter(connect(mapStateToProps)(Itinerary));
