export const days = [
    {
        id: 'day1',
        name: 'Day 1',
    }
]

const allEvents = [
    {
        id: 'event1',
        dayId: 'day1',
        row: '1',
        column: '1',
        type: 'Hotel',
        name: 'Event 1',
        timestamp: 1575699704205
    },
    {
        id: 'event2',
        dayId: 'day1',
        row: '1',
        column: '1',
        type: 'Flight',
        name: 'Event 2',
        timestamp: 1575700057203
    },
    {
        id: 'event3',
        dayId: 'day1',
        row: '2',
        column: '1',
        type: 'Hotel',
        name: 'Event 3',
        timestamp: 1575700095605
    },
    {
        id: 'event4',
        dayId: 'day1',
        row: '2',
        column: '2',
        type: 'Flight',
        name: 'Event 4',
        timestamp: 1575700095705
    },
    {
        id: 'event5',
        dayId: 'day2',
        row: '1',
        column: '2',
        type: 'Cruise',
        name: 'Event 5',
        timestamp: 1575700095805
    },
    {
        id: 'event6',
        dayId: 'day1',
        row: '1',
        column: '2',
        type: 'Cruise',
        name: 'Event 6',
        timestamp: 1575700095805
    },
    {
        id: 'event7',
        dayId: 'day1',
        row: '2',
        column: '2',
        type: 'Cruise',
        name: 'Event 7',
        timestamp: 1575700095805
    },
    {
        id: 'event8',
        dayId: 'day1',
        row: '1',
        column: '3',
        type: 'Train',
        name: 'Event 8',
        timestamp: 1575700095805
    },
    {
        id: 'event9',
        dayId: 'day1',
        row: '2',
        column: '3',
        type: 'Train',
        name: 'Event 9',
        timestamp: 1575700095805
    },
]

export const structureData = ({ id }) => {
    let rows = [];
    let events = allEvents.filter(event => event.dayId === id);

    events.forEach(event => {
        let row = rows[event.row - 1];

        if (!row) row = { type: 'columns', columns: [] };

        let column = row.columns;
        if (!column[event.column - 1]) column[event.column - 1] = { type: 'event', events: []}

        column[event.column - 1].events.push(event);

        row.columns = column;
        rows[event.row - 1] = row;
    });

    return rows;
}


// export const structureData = ({ id, totalCol, totalRow }) => {
//     let rows = {};
//     let events = allEvents.filter(event => event.dayId === id);

//     events.forEach(event => {
//         if (!rows[`row${event.row}`]) 
//             rows[`row${event.row}`] = {};

//         if (!rows[`row${event.row}`][`column${event.column}`]) 
//             rows[`row${event.row}`][`column${event.column}`] = [];

//         rows[`row${event.row}`][`column${event.column}`].push(event);
//     });

//     return rows;
// }

/*

data = {
    'row1': {
        type: 'columns | carousel | etc',
        count: x,
        data: {
                column1: [
                    { events },
                    { events },
                ]
            }
        
        },
    row2: {
        column1: [
            { events },
            { events }, 
        ]
    }
}


data = [
    {
        type: 'columns',
        columns: [
            {
                type: 'events',
                events: [
                    { events },
                    { type: 'Hotel', data: {}, id } 
                ]
            },
            {
                type: 'events',
                events: [
                    { events },
                    { type: 'Hotel', data: {}, id } 
                ]
            }   
        ]
    },
    {
        column1: [
            { events },
            { events },
        ],
        column2: [
            { events },
            { events },
        ],
    }
]

*/

