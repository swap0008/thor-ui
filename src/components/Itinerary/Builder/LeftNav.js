import React, { Component } from 'react';
import { Grid, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { CalendarToday } from '@material-ui/icons';

class LeftNav extends Component {
    render() {
        const { days = [], selectDay } = this.props;

        return (
            <Grid item xs={2}>
                <List>
                    {days.map((id, i) => (
                        <ListItem button onClick={() => selectDay(id)} key={id}>
                            <ListItemIcon>
                                <CalendarToday />
                            </ListItemIcon>
                            <ListItemText primary={`Day ${i + 1}`} />
                        </ListItem>
                    ))}
                </List>
            </Grid>
        );
    }
}

export default LeftNav;