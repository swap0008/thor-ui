import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AddCircleTwoTone, DeleteForever, FilterNone as Clone, Settings, ArrowUpward, ArrowDownward } from '@material-ui/icons';
import { handleDeleteEvent } from '../../../redux/actions/events';

class Event extends Component {
    deleteEvent = () => {
        const { event, col, pos, row, dispatch } = this.props;
        dispatch(handleDeleteEvent({
            id: event.id,
            day_id: event.day_id,
            column: col,
            position: pos,
            row: row
        }));
    }

    handleDragOver = (e) => {
        e.preventDefault();
        e.currentTarget.className += ' on-event-enter';
    }

    handleDragEnter = (e) => {
        e.currentTarget.className += ' on-event-enter';
    }

    handleDragLeave = (e) => {
        e.currentTarget.className = 'builder-card-wrapper';
    }

    handleOnDrop = (e) => {
        e.currentTarget.className = 'builder-card-wrapper';
        const { handleEventDrop, row, col, pos } = this.props;
        handleEventDrop(row, col, pos);
    }

    render() {
        const { event, handleEventDrop, row, col, pos, moveUp, moveDown } = this.props;

        return (
            <div
                className="builder-card-wrapper"
                key={event.id}
                onDragOver={this.handleDragOver}
                onDragEnter={this.handleDragEnter}
                onDragLeave={this.handleDragLeave}
                onDrop={this.handleOnDrop}
            >
                <div className="move-event">
                    <ArrowUpward className="icon" onClick={() => moveUp(row, col, pos)} />
                    <ArrowDownward className="icon" onClick={() => moveDown(row, col, pos)} />
                </div>
                <div className="event-add-icon">
                    <AddCircleTwoTone className="icon" onClick={() => handleEventDrop(row, col, pos + 1)} />
                </div>
                <div className="event-options">
                    <Settings className="icon" />
                    <Clone className="icon" />
                    <DeleteForever className="icon" onClick={this.deleteEvent} />
                </div>
                <div className="image-card">
                    <div className="image-layer" />
                    <div className="bottom-left">
                        <span className="bottom-price">₹ 6,999</span>
                        <span className="bottom-text">{event.name}</span>
                    </div>
                </div>
                <div className="card-info">
                    <div className="description">
                        <span className="address">Nehru Place</span>
                        <span className="location">{event.type}</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect()(Event);