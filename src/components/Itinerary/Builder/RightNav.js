import React, { Component } from 'react';
import SelectRow from './SelectRow';
import SelectEvent from './SelectEvent';
import { ADD_ROW, ADD_EVENT } from './constants';

class RightNav extends Component {
    selectComponent = () => {
        const { rightNav, addRow, addEvent } = this.props;

        switch(rightNav) {
            case ADD_ROW: 
                return <SelectRow addRow={addRow} />;
            case ADD_EVENT:
                return <SelectEvent addEvent={addEvent} />;
            default:
                return '';
        }
    }

    render() {
        return (
            <React.Fragment>
                {this.selectComponent()}
            </React.Fragment>
        );
    }
}

export default RightNav;