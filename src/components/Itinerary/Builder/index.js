import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import {
    AddCircleTwoTone,
    ArrowDownward,
    ArrowUpward,
    FilterNone as Clone,
    DeleteForever,
    Settings
} from '@material-ui/icons';
import RightNav from './RightNav';
import LeftNav from './LeftNav';
import Event from './Event';
import AddCard from '../AddCard';
import { handleFetchEvents, handleSaveEvent } from '../../../redux/actions/events';
import { handleUpdateRow } from '../../../redux/actions/days';
import { ADD_EVENT, ADD_ROW } from './constants';

class Builder extends Component {
    state = {
        dayId: '',
        rightNav: 'none'
    }

    async componentDidMount() {
        const { itinerary, history: { push }, dispatch, match: { params: { lang } } } = this.props;
        console.log(itinerary);
        if (!itinerary) return push(`/${lang}/itineraries`);
        const dayId = itinerary.days_id[0];

        this.setState({ dayId, daysId: itinerary.days_id });
        dispatch(handleFetchEvents(dayId));
    }

    handleAddRow = async (columns) => {
        const body = {
            id: this.state.dayId,
            row: {
                position: this.state.position,
                columns
            },
            type: 'add'
        };

        this.setState({ rightNav: 'none' });
        this.props.dispatch(handleUpdateRow(body));
    };

    handleDeleteRow = async (position) => {
        const body = {
            id: this.state.dayId,
            row: {
                position,
            },
            type: 'delete'
        };

        this.props.dispatch(handleUpdateRow(body));
    }

    handleAddEvent = async (type) => {
        const { dayId, column, row, eventPosition } = this.state;
        const body = {
            name: 'Test',
            type,
            day_id: dayId,
            column,
            row,
            position: eventPosition
        }

        this.setState({ eventPosition: 0, rightNav: 'none' });
        this.props.dispatch(handleSaveEvent(body));
    }

    handleEventDrop = (row, column, position) => {
        this.setState({
            row,
            column,
            eventPosition: position,
            rightNav: ADD_EVENT
        });
    }

    handleMoveRowUp = (position) => {
        const body = {
            id: this.state.dayId,
            row: {
                position
            },
            type: 'move_up'
        };

        this.props.dispatch(handleUpdateRow(body));
    }

    handleMoveRowDown = (position) => {
        const body = {
            id: this.state.dayId,
            row: {
                position
            },
            type: 'move_down'
        };

        this.props.dispatch(handleUpdateRow(body));
    }

    handleMoveEventUp = (row, column, position) => {
        const body = {
            id: this.state.dayId,
            row: {
                number: row,
                column: column,
                position
            },
            type: 'move_event_up'
        };

        this.props.dispatch(handleUpdateRow(body));
    }

    handleMoveEventDown = (row, column, position) => {
        const body = {
            id: this.state.dayId,
            row: {
                number: row,
                column: column,
                position
            },
            type: 'move_event_down'
        };

        this.props.dispatch(handleUpdateRow(body));
    }

    handleSelectDay = (dayId) => {
        this.setState({ dayId });
        this.props.dispatch(handleFetchEvents(dayId));
    }

    render() {
        const { rows } = this.props;

        return (
            <Grid container spacing={1}>
                <LeftNav days={this.state.daysId} selectDay={this.handleSelectDay} />
                <Grid item xs={8} className="builder">
                    {rows.length === 0 && (
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => this.setState({ position: 0, rightNav: ADD_ROW })}
                        >
                            Add Row
                        </Button>
                    )}
                    {rows.map((row, i) => (
                        <div className="row" key={i}>
                            {row.columns.map((column, col) => (
                                <div className="column" key={col}>
                                    {column.events.map((event, index) => (
                                        <Event
                                            event={event}
                                            key={event.id}
                                            col={col}
                                            pos={index}
                                            row={i}
                                            handleEventDrop={this.handleEventDrop}
                                            moveUp={this.handleMoveEventUp}
                                            moveDown={this.handleMoveEventDown}
                                        />
                                    ))}
                                    <Button
                                        variant="contained"
                                        size="small"
                                        color="primary"
                                        onClick={() => this.setState({ column: col, row: i, rightNav: ADD_EVENT })}
                                    >
                                        Add Event
                                    </Button>
                                </div>
                            ))}
                            <div className="move-row">
                                <div className="hold">
                                    <ArrowUpward className="icon" onClick={() => this.handleMoveRowUp(i)} />
                                    <ArrowDownward className="icon" onClick={() => this.handleMoveRowDown(i)} />
                                </div>
                            </div>
                            <div className="row-options">
                                <Settings className="icon" />
                                <Clone className="icon clone" />
                                <DeleteForever
                                    className="icon"
                                    onClick={() => this.handleDeleteRow(i)}
                                />
                            </div>
                            <AddCircleTwoTone
                                className="add-row"
                                onClick={() => this.setState({ position: i + 1, rightNav: ADD_ROW })}
                            />
                        </div>
                    ))}
                    <div id="overlay">
                        <AddCard />
                    </div>
                </Grid>
                <RightNav
                    addRow={this.handleAddRow}
                    addEvent={this.handleAddEvent}
                    rightNav={this.state.rightNav} />
            </Grid>
        );
    }
}

const mapStateToProps = ({ events, itineraries }, props) => {
    const { itineraryId } = props.match.params;

    return {
        rows: events,
        itinerary: itineraries[itineraryId]
    }
}

export default connect(mapStateToProps)(Builder);