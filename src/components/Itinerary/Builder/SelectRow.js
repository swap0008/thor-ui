import React, { Component } from 'react';

class SelectRow extends Component {
    state = {
        columns: ['1 Column', '2 Column', '3 Column', '4 Column', '5 Column', '6 Column']
    }

    render() {
        const { columns } = this.state;

        return (
            <div className="builder-right-nav">
                <div className="add-event">
                    {columns.map((type, i) => (
                        <div className="event-type" key={type} onClick={() => this.props.addRow(i + 1)}>
                            {type}
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default SelectRow;