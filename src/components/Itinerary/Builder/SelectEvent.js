import React, { Component } from 'react';
import { Grid } from '@material-ui/core';

class SelectEvent extends Component {
    state = {
        eventsType: ['Hotel', 'Train', 'Flights', 'Cruise']
    }

    render() {
        const { eventsType } = this.state;

        return (
            <Grid item xs={2} className="builder-right-nav">
                <div className="add-event">
                    {eventsType.map(type => <div 
                        className="event-type" 
                        key={type} 
                        draggable={true}
                        onDragEnd={() => this.props.addEvent(type)}
                        onClick={() => this.props.addEvent(type)}>{type}</div>)}
                </div>
            </Grid>
        );
    }
}

export default SelectEvent;