import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { tripCardDetails as messages } from './data';

const TripCardDetails = (props) => {
	const { intl: { formatMessage } } = props;
	const { name = ' ', currency = ' ', cost = ' ', small_description = ' ' } = props.itinerary || {};

	return (
		<React.Fragment>
			<span className="location">{name}</span>
			<span className="location">{small_description}</span>
			<span className="days-nights">
				8 {formatMessage(messages[0])} 7 {formatMessage(messages[1])}
			</span>
			<span className="estimated-cost">{formatMessage(messages[2])}: {cost} {currency}</span>
			<span className="total-proposal">{formatMessage(messages[3])}: 10</span>
			<span className="active-proposal">{formatMessage(messages[4])}: 3</span>
			<div className="active-proposal-list">
				<span className="heading">{formatMessage(messages[5])} 1</span>
				<span>{formatMessage(messages[6])}:</span>
				<span>{formatMessage(messages[7])}:</span>
				<span>{formatMessage(messages[8])}: $1200</span>
				<span>
					9 {formatMessage(messages[0])} 8 {formatMessage(messages[1])}
				</span>
				<span>23 Dec, 2019 to 31 Dec, 2019</span>
			</div>
		</React.Fragment>
	);
};

const mapStateToProps = ({ itineraries }, props) => {
	const { tripId } = props.match.params;
	
	return {
		itinerary: itineraries[tripId]
	}
}

export default injectIntl(connect(mapStateToProps)(TripCardDetails));
