import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TextField, Button } from '@material-ui/core';
import { handleSaveItinerary } from '../../redux/actions/itineraries';

class AddCard extends Component {
    state = {};

    componentDidMount() {
        this.overlay = document.querySelector('#overlay');
    }

    handleInputs = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleAddCard = () => {
        const { dispatch } = this.props;

        dispatch(handleSaveItinerary(this.state));
    }

    render() {
        const {
            name = '',
            small_description = '',
            language = '',
            currency = '',
            cost = ''
        } = this.state;

        return (
            <form noValidate autoComplete="off" className="form">
                <h2>Add Card</h2>
                <TextField fullWidth label="Name" value={name} name="name" onChange={this.handleInputs} /><br />
                <TextField fullWidth label="Description" value={small_description} name="small_description" onChange={this.handleInputs} /><br />
                <TextField fullWidth label="Language" value={language} name="language" onChange={this.handleInputs} /><br />
                <TextField fullWidth label="Currency" value={currency} name="currency" onChange={this.handleInputs} /><br />
                <TextField fullWidth label="Cost" value={cost} name="cost" onChange={this.handleInputs} /><br /><br />
                <Button variant="contained" color="primary" style={{ padding: 7 }} onClick={this.handleAddCard}>
                    Save
				</Button>
                <Button
                    variant="contained"
                    style={{ marginLeft: 10 }}
                    onClick={() => this.overlay.style.display = 'none'}
                >
                    Close
				</Button>
            </form>
        );
    }
}

export default connect()(AddCard);