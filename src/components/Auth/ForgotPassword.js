import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import Axios from 'axios';
import serializeForm from 'form-serialize';

class ForgotPassword extends Component {
    handleForgotPassword = async (e) => {
        e.preventDefault();
        const { match, history } = this.props;

        const { email } = serializeForm(e.target, { hash: true })

        const verificationURL = await Axios({
            method: 'get',
            url: `http://localhost:4001/verification-url/${email}?type=RESET_PASSWORD`
        });

        const { data } = await Axios.get(verificationURL.data);

        if (data.token) {
            localStorage.setItem('token', data.token);
            history.push(`/${match.params.lang}/reset-password`);
        } else {
            alert('Something went wrong');
        }
    }

    render() {
        const { match } = this.props;

        return (
            <div className="account-body accountbg">
                <div className="row vh-100 ">
                    <div className="col-12 align-self-center">
                        <div className="auth-page">
                            <div className="card auth-card shadow-lg">
                                <div className="card-body">
                                    <div className="px-3">
                                        <div className="auth-logo-box">
                                            <a href="../analytics/analytics-index.html" className="logo logo-admin">
                                                <img src="../assets/images/logo-sm.png" height="55" alt="logo" className="auth-logo" />
                                            </a>
                                        </div>

                                        <div className="text-center auth-logo-text">
                                            <h4 className="mt-0 mb-3 mt-5">Reset Password</h4>
                                            <p className="text-muted mb-0">Enter your Email and instructions will be sent to you!</p>
                                        </div>


                                        <form className="form-horizontal auth-form my-4" method="post" onSubmit={this.handleForgotPassword}>

                                            <div className="form-group">
                                                <label>Email</label>
                                                <div className="input-group mb-3">
                                                    <span className="auth-form-icon">
                                                        <i className="dripicons-mail"></i>
                                                    </span>
                                                    <input type="email" name="email" className="form-control" id="useremail" placeholder="Enter Email" />
                                                </div>
                                            </div>


                                            <div className="form-group mb-0 row">
                                                <div className="col-12 mt-2">
                                                    <button className="btn btn-gradient-primary btn-round btn-block waves-effect waves-light" type="submit">Reset <i className="fas fa-sign-in-alt ml-1"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div className="m-3 text-center text-muted">
                                        <p className="">Remember It ?
                                            <Link to={`/${match.params.lang}/login`} className="text-primary ml-2">Sign in here</Link>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(ForgotPassword);