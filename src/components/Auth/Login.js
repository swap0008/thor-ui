import React, { Component } from 'react';
import { useForm } from 'react-hook-form';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import serializeForm from 'form-serialize';
import { handleLogin } from '../../redux/actions/auth';

const PUBLIC_URL = process.env.PUBLIC_URL;

const LoginForm = ({ onSubmit, lang, me }) => {
    const { handleSubmit, register, errors } = useForm();

    return (
        <form className="form-horizontal auth-form my-4" onSubmit={handleSubmit(onSubmit)}>

            <div className="form-group form-group-container">
                <label htmlFor="username">Email</label>
                <div className="input-group mb-3">
                    <span className="auth-form-icon">
                        <i className="dripicons-user"></i>
                    </span>
                    <input type="email" name="email" className={`form-control ${errors.email ? 'form-field-error' : ''}`} id="username" placeholder="Enter email"
                        ref={register({
                            required: true,
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                message: 'Invalid email address'
                            }
                        })}
                        autoComplete="off"
                    />
                </div>
                <span className="form-field-error-text">{errors.email && errors.email.message}</span>
            </div>

            <div className="form-group form-group-container">
                <label htmlFor="userpassword">Password</label>
                <div className="input-group mb-3">
                    <span className="auth-form-icon">
                        <i className="dripicons-lock"></i>
                    </span>
                    <input type="password" name="password" className={`form-control ${errors.password ? 'form-field-error' : ''}`} id="userpassword" placeholder="Enter password"
                        ref={register({
                            required: true,
                            minLength: { value: 6, message: 'minimum password length should be 6' },
                        })}
                    />
                </div>
                <span className="form-field-error-text">{errors.password ? errors.password.message : ''}</span>
            </div>

            <div className="form-group row mt-4">
                <div className="col-sm-12 text-right">
                    <Link to={`/${lang}/forgot-password`} className="text-muted font-13">
                        <i className="dripicons-lock"></i> Forgot password?
                    </Link>
                </div>
            </div>

            <div className="form-group mb-0 row">
                <div className="col-12 mt-2">
                    <button className="btn btn-gradient-primary btn-round btn-block waves-effect waves-light" type="submit" disabled={me.isFetching}>
                        {me.isFetching ? (
                                <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            ) : (
                                <>Log In <i className="fas fa-sign-in-alt ml-1"></i></>
                        )}
                    </button>
                </div>
            </div>
        </form>
    );
}

class Login extends Component {
    handleSubmit = (values) => {
        this.props.dispatch(handleLogin(values, this.props));
    }

    render() {
        const { match: { params: { lang } }, me } = this.props;

        return (
            <div className="account-body accountbg">
                <div className="row vh-100 ">
                    <div className="col-12 align-self-center">
                        <div className="auth-page">
                            <div className="card auth-card shadow-lg">
                                <div className="card-body">
                                    <div className="px-3">
                                        <div className="auth-logo-box">
                                            <a href={`${PUBLIC_URL}/analytics/analytics-index.html`} className="logo logo-admin">
                                                <img src="../assets/images/logo-sm.png" height="55" alt="logo" className="auth-logo" />
                                            </a>
                                        </div>

                                        <div className="text-center auth-logo-text">
                                            <h4 className="mt-0 mb-3 mt-5">Sign In</h4>
                                            <p className="text-muted mb-0">Sign in to continue.</p>
                                        </div>


                                        <LoginForm onSubmit={this.handleSubmit} lang={lang} me={me} />
                                    </div>

                                    <div className="m-3 text-center text-muted">
                                        <p className="">Don't have an account ?  <Link to={`/${lang}/register`} className="text-primary ml-2">Free Resister</Link></p>
                                    </div>
                                </div>
                            </div>
                            <div className="account-social text-center mt-4">
                                <h6 className="my-4">Or Login With</h6>
                                <ul className="list-inline mb-4">
                                    <li className="list-inline-item">
                                        <a href="" className="">
                                            <i className="fab fa-facebook-f facebook"></i>
                                        </a>
                                    </li>
                                    <li className="list-inline-item">
                                        <a href="" className="">
                                            <i className="fab fa-twitter twitter"></i>
                                        </a>
                                    </li>
                                    <li className="list-inline-item">
                                        <a href="" className="">
                                            <i className="fab fa-google google"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ me }) => {
    return {
        me
    }
}

export default withRouter(connect(mapStateToProps)(Login));