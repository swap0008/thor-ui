import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { connect } from 'react-redux';
import serializeForm from 'form-serialize';
import { handleSignUp } from '../../redux/actions/auth';

const PUBLIC_URL = process.env.PUBLIC_URL;

const SignUpForm = ({ onSubmit, me }) => {
    const { handleSubmit, register, errors } = useForm();

    return (
        <form className="form-horizontal auth-form my-4" onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group form-group-container">
                <label for="username">Name</label>
                <div className="input-group mb-3">
                    <span className="auth-form-icon">
                        <i className="dripicons-user"></i>
                    </span>
                    <input 
                        type="text" 
                        name="name" 
                        className={`form-control ${errors.name ? 'form-field-error' : ''}`} 
                        id="username" 
                        placeholder="Enter name"
                        autoComplete="off"
                        ref={register({
                            required: true,
                            pattern: {
                                value: /^([a-zA-Z0-9]+|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{1,}|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{3,}\s{1}[a-zA-Z0-9]{1,})$/g,
                                message: "enter valid name"
                            }
                        })}
                    />
                </div>
                <span className="form-field-error-text">{errors.name && errors.name.message}</span>
            </div>

            <div className="form-group form-group-container">
                <label for="useremail">Email</label>
                <div className="input-group mb-3">
                    <span className="auth-form-icon">
                        <i className="dripicons-mail"></i>
                    </span>
                    <input 
                        type="email" 
                        name="email" 
                        className={`form-control ${errors.email ? 'form-field-error' : ''}`} 
                        id="useremail" 
                        placeholder="Enter Email"
                        autoComplete="off"
                        ref={register({
                            required: true,
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                message: 'Invalid email address'
                            }
                        })}
                    />
                </div>
                <span className="form-field-error-text">{errors.email && errors.email.message}</span>
            </div>

            <div className="form-group form-group-container">
                <label for="userpassword">Password</label>
                <div className="input-group mb-3">
                    <span className="auth-form-icon">
                        <i className="dripicons-lock"></i>
                    </span>
                    <input 
                        type="password" 
                        name="password" 
                        className="form-control"
                        className={`form-control ${errors.password ? 'form-field-error' : ''}`} 
                        id="userpassword" 
                        placeholder="Enter password"
                        ref={register({
                            required: true,
                            minLength: { value: 6, message: 'minimum password length should be 6' },
                        })}
                    />
                </div>
                <span className="form-field-error-text">{errors.password && errors.password.message}</span>
            </div>

            <div className="form-group row mt-4">
                <div className="col-sm-12">
                    <div className="custom-control custom-switch switch-success">
                        <input type="checkbox" className="custom-control-input" id="customSwitchSuccess" />
                        <label className="custom-control-label text-muted" for="customSwitchSuccess">
                            By registering you agree to the Frogetor <a href="#" className="text-primary">Terms of Use</a>
                        </label>
                    </div>
                </div>
            </div>

            <div className="form-group mb-0 row">
                <div className="col-12 mt-2">
                    <button className="btn btn-gradient-primary btn-round btn-block waves-effect waves-light" type="submit" disabled={me.isFetching}>
                        {me.isFetching ? (
                                <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            ) : (
                                <>Register <i className="fas fa-sign-in-alt ml-1"></i></>
                        )}
                    </button>
                </div>
            </div>
        </form>
    );
}

class SignUp extends Component {
    handleSubmit = (data) => {
        this.props.dispatch(handleSignUp(data, this.props));
    }

    render() {
        const { match: { params: { lang } }, me } = this.props;

        return (
            <div className="account-body accountbg">
                <div className="row vh-100 ">
                    <div className="col-12 align-self-center">
                        <div className="auth-page">
                            <div className="card auth-card shadow-lg">
                                <div className="card-body">
                                    <div className="px-3">
                                        <div className="auth-logo-box">
                                            <a href={`${PUBLIC_URL}/analytics/analytics-index.html`} className="logo logo-admin">
                                                <img src="../assets/images/logo-sm.png" height="55" alt="logo" className="auth-logo" />
                                            </a>
                                        </div>

                                        <div className="text-center auth-logo-text">
                                            <h4 className="mt-0 mb-3 mt-5">Sign Up</h4>
                                            <p className="text-muted mb-0">Get your free account now.</p>
                                        </div>


                                        <SignUpForm onSubmit={this.handleSubmit} me={me} />
                                    </div>

                                    <div className="m-3 text-center text-muted">
                                        <p className="">Already have an account ? <Link to={`/${lang}/login`} className="text-primary ml-2">Log in</Link></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ me }) => {
    return {
        me
    }
}

export default withRouter(connect(mapStateToProps)(SignUp));