import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import Axios from 'axios';
import serializeForm from 'form-serialize';

class ResetPassword extends Component {
    handleResetPassword = async (e) => {
        e.preventDefault();
        const { match, history } = this.props;

        const { password, confirm_password } = serializeForm(e.target, { hash: true });

        if (password !== confirm_password) {
            return alert('Password does not match');
        }

        const result = await Axios({
            method: 'post',
            url: `http://localhost:4001/reset-password`,
            data: {
                password
            },
            headers: {
                'x-auth-token': localStorage.getItem('token')
            }
        });

        if (result.status === 200) {
            alert('Password Changed!');
            localStorage.setItem('token', '');
            history.push(`/${match.params.lang}/login`);
        }
    }

    render() {
        const { match } = this.props;

        return (
            <div className="account-body accountbg">
                <div className="row vh-100 ">
                    <div className="col-12 align-self-center">
                        <div className="auth-page">
                            <div className="card auth-card shadow-lg">
                                <div className="card-body">
                                    <div className="px-3">
                                        <div className="auth-logo-box">
                                            <a href="../analytics/analytics-index.html" className="logo logo-admin">
                                                <img src="../assets/images/logo-sm.png" height="55" alt="logo" className="auth-logo" />
                                            </a>
                                        </div>

                                        <div className="text-center auth-logo-text">
                                            <h4 className="mt-0 mb-3 mt-5">Reset Password</h4>
                                            <p className="text-muted mb-0">Enter your new password.</p>
                                        </div>


                                        <form className="form-horizontal auth-form my-4" method="post" onSubmit={this.handleResetPassword}>

                                            <div className="form-group">
                                                <label>New Password</label>
                                                <div className="input-group mb-3">
                                                    <span className="auth-form-icon">
                                                        <i className="dripicons-mail"></i>
                                                    </span>
                                                    <input type="password" name="password" className="form-control" placeholder="Password" />
                                                </div>
                                            </div>

                                            <div className="form-group">
                                                <label>Confirm Password</label>
                                                <div className="input-group mb-3">
                                                    <span className="auth-form-icon">
                                                        <i className="dripicons-mail"></i>
                                                    </span>
                                                    <input type="password" name="confirm_password" className="form-control" placeholder="Confirm Password" />
                                                </div>
                                            </div>


                                            <div className="form-group mb-0 row">
                                                <div className="col-12 mt-2">
                                                    <button className="btn btn-gradient-primary btn-round btn-block waves-effect waves-light" type="submit">Reset <i className="fas fa-sign-in-alt ml-1"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div className="m-3 text-center text-muted">
                                        <p className="">Remember It ?
                                            <Link to={`/${match.params.lang}/login`} className="text-primary ml-2">Sign in here</Link>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(ResetPassword);