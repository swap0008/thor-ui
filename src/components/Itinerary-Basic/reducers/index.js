import {
    FETCHED_ITINERARIES_BASIC,
    FETCHING_ITINERARIES_BASIC,
    SAVE_ITINERARY_BASIC,
    UPDATE_ITINERARY_BASIC,
    DELETE_ITINERARY_BASIC
} from '../actions/actionType';

export default function (state = {}, action) {
    const { itinerary, itineraryId, isFetching } = action;

    switch (action.type) {
        case FETCHED_ITINERARIES_BASIC:
            return {
                ...state,
                data: action.itineraries,
                isFetching
            }
        case FETCHING_ITINERARIES_BASIC:
            return {
                ...state,
                isFetching
            }
        case SAVE_ITINERARY_BASIC:
            return {
                ...state,
                data: {
                    ...state['data'],
                    [itinerary.id]: itinerary
                },
                isFetching
            }
        case UPDATE_ITINERARY_BASIC:
            return {
                ...state,
                data: {
                    ...state['data'],
                    [itinerary.id]: {
                        ...state['data'][itinerary.id],
                        ...itinerary
                    }
                },
                isFetching
            }
        case DELETE_ITINERARY_BASIC:
            delete state['data'][itineraryId];
            return { ...state, isFetching: action.isFetching };
        default:
            return state;
    }
}