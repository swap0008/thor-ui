import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { handleFetchItineraries } from '../actions';
import Itineraries from './Itineraries';
import AddItinerary from './AddItinerary';
import EditItinerary from './EditItinerary';

class ItineraryBasic extends Component {
    componentDidMount() {
        const { businessId } = this.props.match.params;

        this.props.dispatch(handleFetchItineraries(businessId, 'name,price,currency,photos,slug'));
    }

    render() {
        return (
            <div className="page-content">
                <Route path="/:lang/:businessId/applications/itinerary/edit/:slug" component={EditItinerary} />
                <Route path="/:lang/:businessId/applications/itinerary/add" component={AddItinerary} />
                <Route exact path="/:lang/:businessId/applications/itinerary" component={Itineraries} />
            </div>
        );
    }
}

export default withRouter(connect()(ItineraryBasic));