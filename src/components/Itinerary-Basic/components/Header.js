import React from 'react';

const Header = (props) => (
    <div className="row">
        <div className="col-sm-12">
            <div className="page-title-box">
                <div className="float-right">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="javascript:void(0);">Thor UI</a></li>
                        <li className="breadcrumb-item"><a href="javascript:void(0);">Applications</a></li>
                        <li className="breadcrumb-item active">Itineraries</li>
                    </ol>
                </div>
                <h4 className="page-title">Products</h4>
            </div>
        </div>
    </div>
);

export default Header;