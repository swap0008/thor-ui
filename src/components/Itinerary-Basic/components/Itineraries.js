import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ItineraryCard from './ItineraryCard';
import Header from './Header';
import { handleDeleteItinerary } from '../actions';
import Loader from './Loader';

class Itineraries extends Component {
    state = {
        searchResults: []
    }

    handleAddItinerary = () => {
        const { match, history } = this.props;

        history.push(`/${match.params.lang}/${match.params.businessId}/applications/itinerary/add`);
    }

    handleRemoveItinerary = (id) => {
        const { businessId } = this.props.match.params;
        this.props.dispatch(handleDeleteItinerary(businessId, id));
    }

    handleEditItinerary = (slug) => {
        const { match: { params: { lang, businessId } }, history } = this.props;

        history.push(`/${lang}/${businessId}/applications/itinerary/edit/${slug}`);
    }

    handleSearch = (query) => {
        if (!query) return this.setState({ searchResults: [] });

        this.setState({
            searchResults: Object.values(this.props.itinerariesBasic.data)
                .filter(itinerary => itinerary.name.toLowerCase().includes(query.toLowerCase()))
        });
    }

    render() {
        const { searchResults } = this.state;
        const { itinerariesBasic } = this.props;

        if (itinerariesBasic.isFetching) {
            return <Loader />
        }
        
        return (
            <div className="container">
                <Header />
                <div className="row">
                    <div className="col-lg-6">
                        <ul className="list-inline">
                            <li className="list-inline-item">
                                <h5 className="mt-0"><span class="badge badge-pink">Itineraries</span></h5>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-6 text-right">
                        <div className="text-right">
                            <ul className="list-inline">
                                <li className="list-inline-item">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            id="example-input1-group2"
                                            name="example-input1-group2"
                                            className="form-control"
                                            placeholder="Search"
                                            onChange={(e) => this.handleSearch(e.target.value)}
                                        />
                                        <span className="input-group-append">
                                            <button type="button" className="btn btn-gradient-primary">
                                                <i className="fas fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </li>
                                <li className="list-inline-item">
                                    <button type="button" className="btn btn-gradient-primary">
                                        <i className="dripicons-gear"></i>
                                    </button>
                                </li>
                                <li className="list-inline-item">
                                    <button type="button" className="btn btn-gradient-primary" onClick={this.handleAddItinerary}>
                                        <i className="mdi mdi-plus-circle"></i> Add Itinerary
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="row">
                    {!Object.keys(itinerariesBasic.data || {}).length && <h4>No itinerary.</h4>}
                    {searchResults.length ? (
                        searchResults.map(result => (
                            <ItineraryCard
                                itinerary={result}
                                deleteItinerary={this.handleRemoveItinerary}
                                editItinerary={this.handleEditItinerary}
                                key={result.id}
                            />
                        ))
                    ) : (
                        itinerariesBasic.data && Object.keys(itinerariesBasic.data).map(id => (
                            <ItineraryCard
                                itinerary={itinerariesBasic.data[id]}
                                deleteItinerary={this.handleRemoveItinerary}
                                editItinerary={this.handleEditItinerary}
                                key={id}
                            />
                        ))
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ itinerariesBasic }) => {
    return {
        itinerariesBasic
    }
}

export default withRouter(connect(mapStateToProps)(Itineraries));