import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import serializeForm from 'form-serialize';
import { useForm } from 'react-hook-form';
import Header from './Header';
import { handleSaveItinerary } from '../actions';

const ItineraryBasicForm = ({}) => {
    const { handleSubmit, register, errors } = useForm();
}

class AddItinerary extends Component {
    state = {
        fields: [
            { display: 'Name', name: 'name', type: 'text', placeholder: 'Enter name' },
            { display: 'Price', name: 'price', type: 'text', placeholder: 'Enter price' },
            { display: 'Currency', name: 'currency', type: 'text', placeholder: 'eg. INR, USD' },
            { display: 'Duration', name: 'duration', type: 'text', placeholder: 'eg. 3 days' },
            { display: 'Travel Date Type', name: 'travel_date_type', type: 'text', placeholder: 'flexible or fixed' },
            { display: 'Cancellation Terms', name: 'cancellation_terms', type: 'text', placeholder: 'Enter termns' },
            { display: 'Terms and Policy', name: 'terms_and_policy', type: 'text', placeholder: 'Enter terms' }
        ]
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { dispatch, match: { params } } = this.props;
        const data = serializeForm(e.target, { hash: true });

        data['business_id'] = params.businessId;

        dispatch(handleSaveItinerary(data));
        this.handleClose();
    }

    handleClose = () => {
        const { match, history } = this.props;

        history.push(`/${match.params.lang}/${match.params.businessId}/applications/itinerary`);
    }

    render() {
        const { fields } = this.state;
        const { isFetching } = this.props;

        return (
            <div className="container-fluid">
                <Header />
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="mt-0 header-title">Add Itinerary</h4>
                                <div className="general-label">
                                    <form method="post" onSubmit={this.handleSubmit}>
                                        {fields.map(field => (
                                            <div className="form-group row" key={field.name}>
                                                <label className="col-sm-2 col-form-label">{field.display}</label>
                                                <div className="col-sm-10">
                                                    <input type={field.type} className="form-control" name={field.name} placeholder={field.placeholder} required/>
                                                </div>
                                            </div>
                                        ))}

                                        <div className="row">
                                            <div className="col-sm-10 ml-auto">
                                                <button type="submit" className="btn btn-gradient-primary btn-margin-right" disabled={isFetching}>
                                                    {isFetching ? (
                                                            <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true">
                                                            </span>
                                                        ) : (
                                                            'Add'
                                                    )}
                                                </button>
                                                <button type="button" className="btn btn-gradient-danger" onClick={this.handleClose}>
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ itinerariesBasic }) => {
    return {
        isFetching: itinerariesBasic.isFetching
    }
}

export default withRouter(connect(mapStateToProps)(AddItinerary));