import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import serializeForm from 'form-serialize';
import Header from './Header';
import { handleUpdateItinerary, handlePhotoUpload, axiosInstance } from '../actions';
import Loader from './Loader';

class EditItinerary extends Component {
    state = {
        fields: [
            { display: 'Name', name: 'name', type: 'text', placeholder: 'Enter name' },
            { display: 'Price', name: 'price', type: 'text', placeholder: 'Enter price' },
            { display: 'Currency', name: 'currency', type: 'text', placeholder: 'eg. INR, USD' },
            { display: 'Duration', name: 'duration', type: 'text', placeholder: 'eg. 3 days' },
            { display: 'Travel Date Type', name: 'travel_date_type', type: 'text', placeholder: 'flexible or fixed' },
            { display: 'Cancellation Terms', name: 'cancellation_terms', type: 'text', placeholder: 'Enter termns' },
            { display: 'Terms and Policy', name: 'terms_and_policy', type: 'text', placeholder: 'Enter terms' }
        ],
        itinerary: {}
    }

    componentDidMount() {
        this.handleFetchItinerary();
    }

    handleFetchItinerary = async () => {
        const { businessId, slug} = this.props.match.params;
        this.setState({ isFetching: true });

        const { data } = await axiosInstance.get(`/${businessId}?slug=${slug}`, {
            headers: {'x-auth-token': localStorage.getItem('token')}
        });

        this.setState({ isFetching: false, itinerary: data });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { dispatch, match: { params } } = this.props;

        const data = serializeForm(e.target, { hash: true });

        data['itinerary_id'] = this.state.itinerary.id;

        dispatch(handleUpdateItinerary(params.businessId, data));
        this.handleClose();
    }

    handleClose = () => {
        const { match, history } = this.props;

        history.push(`/${match.params.lang}/${match.params.businessId}/applications/itinerary`);
    }

    photoUpload = (e) => {
        e.preventDefault();
        const { itinerary, dispatch, match: { params } } = this.props;

        const files = Array.from(e.target.files);
        dispatch(handlePhotoUpload(files, params.businessId, itinerary.id));
    }

    render() {
        const { fields, itinerary, isFetching } = this.state;

        return (
            <div className="container-fluid">
                <Header />
                {isFetching && <Loader position="fixed" />}
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="mt-0 header-title">Edit Itinerary</h4>
                                <div className="general-label">
                                    <form method="post" onSubmit={this.handleSubmit}>
                                        {fields.map(field => (
                                            <div className="form-group row" key={field.name}>
                                                <label className="col-sm-2 col-form-label">{field.display}</label>
                                                <div className="col-sm-10">
                                                    <input
                                                        type={field.type}
                                                        className="form-control"
                                                        name={field.name}
                                                        defaultValue={itinerary[field.name] || ''}
                                                        placeholder={field.placeholder}
                                                        required />
                                                </div>
                                            </div>
                                        ))}

                                        <div className="form-group row">
                                            <label className="col-sm-2 col-form-label">Upload Photo</label>
                                            <div className="col-sm-10">
                                                <input
                                                    type="file"
                                                    className="form-control"
                                                    name="photo"
                                                    placeholder="Choose photo"
                                                    onChange={this.photoUpload}
                                                />
                                            </div>
                                        </div>



                                        <div className="row">
                                            <div className="col-sm-10 ml-auto">
                                                <button type="submit" className="btn btn-gradient-primary btn-margin-right">Update</button>
                                                <button type="button" className="btn btn-gradient-danger" onClick={this.handleClose}>
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ itinerariesBasic }, { match }) => {
    return {
       
    }
}

export default withRouter(connect(mapStateToProps)(EditItinerary));