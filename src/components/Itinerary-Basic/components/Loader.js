import React from 'react';

const styles ={
	loader: {
		width: '100%',
		textAlign: 'center',
		marginTop: '10%',
		zIndex: 999
	}
}

const Loader = (props) => (
	<div style={{ ...styles.loader, ...props}}>
		<div class="spinner-border text-primary" role="status">
		</div>
	</div>
);

export default Loader;