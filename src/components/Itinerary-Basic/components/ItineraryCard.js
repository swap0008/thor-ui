import React from 'react';

const randomPhoto = 'https://images.unsplash.com/photo-1499678329028-101435549a4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80';

const ItineraryCard = ({ itinerary, deleteItinerary, editItinerary }) => (
    <div className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div className="card e-co-product">
            <a href="">
                <img src={itinerary.photos ? itinerary.photos[0] : randomPhoto} alt="" className="img-fluid" />
            </a>
            <div className="card-body product-info">
                <a href="" className="product-title">{itinerary.name || ''}</a>
                <div className="d-flex justify-content-between my-2">
                    <p className="product-price">{itinerary.price} {itinerary.currency} <span className="ml-2"><del>$49.00</del></span></p>
                    {/* <ul className="list-inline mb-0 product-review align-self-center">
                        <li className="list-inline-item"><i className="mdi mdi-star text-warning"></i></li>
                        <li className="list-inline-item"><i className="mdi mdi-star text-warning"></i></li>
                        <li className="list-inline-item"><i className="mdi mdi-star text-warning"></i></li>
                        <li className="list-inline-item"><i className="mdi mdi-star text-warning"></i></li>
                        <li className="list-inline-item"><i className="mdi mdi-star-half text-warning"></i></li>
                    </ul> */}
                </div>
                <button className="btn btn-gradient-primary btn-round px-3 btn-sm waves-effect waves-light btn-margin-right">
                    View
                </button>
                <button
                    className="btn btn-gradient-pink btn-sm waves-effect waves-light btn-round btn-margin-right"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Wishlist"
                    onClick={() => editItinerary(itinerary.slug)}
                >
                    <i className="mdi mdi-pencil-outline" style={{ fontSize: 16 }}></i>
                </button>
                <button
                    className="btn btn-gradient-secondary  btn-sm waves-effect waves-light btn-round"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Delete"
                    onClick={() => deleteItinerary(itinerary.id)}
                >
                    <i className="mdi mdi-delete" style={{ fontSize: 18 }}></i>
                </button>
            </div>
        </div>
    </div>
);

export default ItineraryCard;