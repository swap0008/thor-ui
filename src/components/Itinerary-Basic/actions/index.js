import Axios from 'axios';
import {
    FETCHING_ITINERARIES_BASIC,
    FETCHED_ITINERARIES_BASIC,
    SAVE_ITINERARY_BASIC,
    UPDATE_ITINERARY_BASIC,
    DELETE_ITINERARY_BASIC,
    PHOTO_UPLOAD_ITINERARY_BASIC
} from './actionType';

// export const axiosInstance = Axios.create({
//     baseURL: 'https://rt29mqfxv0.execute-api.ap-south-1.amazonaws.com/dev'
// });

export const axiosInstance = window.axiosInstances['itinerary_basic'];

const setHeaders = () => {
    const token = localStorage.getItem('token');

    return {
        'x-auth-token': token
    };
}

const fetchedItineraries = (itineraries) => {
    return {
        type: FETCHED_ITINERARIES_BASIC,
        itineraries,
        isFetching: false
    }
}

const fetchingItineraries = () => {
    return {
        type: FETCHING_ITINERARIES_BASIC,
        isFetching: true
    }
}

export const handleFetchItineraries = (businessId, fields) => {
    return async (dispatch) => {
        try {
            dispatch(fetchingItineraries());
            const { data } = await axiosInstance.get(`/${businessId}?fields=${fields}`, { headers: setHeaders() });
            dispatch(fetchedItineraries(data));
        } catch (err) {
            console.log(err);
        }
    }
}

const saveItinerary = (itinerary) => {
    return {
        type: SAVE_ITINERARY_BASIC,
        itinerary,
        isFetching: false
    }
}

export const handleSaveItinerary = (itinerary) => {
    return async (dispatch) => {
        try {
            dispatch(fetchingItineraries());
            const { data } = await axiosInstance.post('/', itinerary, { headers: setHeaders() });
            dispatch(saveItinerary(data));
        } catch (err) {
            console.log(err);
        }
    }
}

const updateItinerary = (itinerary) => {
    return {
        type: UPDATE_ITINERARY_BASIC,
        itinerary,
        isFetching: false
    }
}

export const handleUpdateItinerary = (businessId, itinerary) => {
    return async (dispatch) => {
        try {
            dispatch(fetchingItineraries());
            const { data } = await axiosInstance.put(`/${businessId}`, itinerary, { headers: setHeaders() });
            dispatch(updateItinerary(data));
        } catch (err) {
            console.log(err);
        }
    }
}

const deleteItinerary = (itineraryId) => {
    return {
        type: DELETE_ITINERARY_BASIC,
        itineraryId
    }
}

export const handleDeleteItinerary = (businessId, itinerary_id) => {
    return async (dispatch) => {
        try {
            const { data } = await axiosInstance.delete(`/${businessId}`, {
                data: { itinerary_id },
                headers: setHeaders()
            });
            dispatch(deleteItinerary(data.itineraryId));
        } catch (err) {
            console.log(err);
        }
    }
}

const photoUpload = (photoUrl) => {
    return {
        type: PHOTO_UPLOAD_ITINERARY_BASIC,
        photoUrl
    };
}

export const handlePhotoUpload = (files, businessId, itineraryId) => {
    return async (dispatch) => {
        for (const file of files) {
            const reader = new FileReader();
            const photos = [];

            reader.addEventListener('loadend', async (e) => {
                const { data: { uploadURL } } = await axiosInstance.post('/s3-signed-url', {
                    name: file.name,
                    type: file.type
                });

                await Axios.put(uploadURL, new Blob([reader.result], { type: file.type }));
                
                photos.push(`https://itinerary-photos.s3.ap-south-1.amazonaws.com/${file.name}`);
                dispatch(handleUpdateItinerary(businessId, {
                    itinerary_id: itineraryId,
                    photos
                }));
            })

            reader.readAsArrayBuffer(file);
        }
    }
}