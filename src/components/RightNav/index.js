import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import ItineraryTripCardDetails from '../Itinerary/TripCardDetails';
import ProposalTripTableDetails from '../Proposal/TripTableDetails';
import ShowCalendars from '../Calendar/ShowCalendars';

class RightNav extends Component {
	render() {
		return (
			<div className="right-nav">
				<Route path="/:lang/itineraries/:tripId" component={ItineraryTripCardDetails} />
				<Route path="/:lang/proposals/:tripId" component={ProposalTripTableDetails} />
				<Route path="/:lang/calendar" component={ShowCalendars} />
			</div>
		);
	}
}

export default RightNav;
