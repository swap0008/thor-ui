import React, { Component } from 'react';

class Three extends Component {
    render() {
        return (
            <div className="hotel-card-wrapper">
                <div className="image-card">
                    <div className="image-layer" />
                    <div className="bottom-left">
                        <span className="bottom-price">₹ 6,999</span>
                        <span className="bottom-text">Eros Hotel New Delhi, Nehru Place</span>
                    </div>
                </div>
                <div className="card-info">
                    <span className="rating">Rating 5/5</span>
                    <div className="description">
                        <span className="address">Nehru Place</span>
                        <span className="location">New Delhi</span>
                    </div>
                    <div className="space"></div>
                </div>
            </div>
        );
    }
}

export default Three;