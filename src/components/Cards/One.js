import React, { Component } from 'react';
import { HotelTwoTone, TrainTwoTone, DirectionsBoatTwoTone, FlightTwoTone, Edit } from '@material-ui/icons';

class One extends Component {
    render() {
        return (
            <div className="card-wrapper">
                <div className="location-image">
                    <div className="image-layer" />
                    <span className="image-center">France</span>
                </div>
                <span className="type">Itinerary</span>
                <h3>France, Belgium</h3>
                <div className="icons-days">
                    <div className="icons">
                        <HotelTwoTone fontSize="small" />
                        <TrainTwoTone fontSize="small" />
                        <DirectionsBoatTwoTone fontSize="small" />
                        <FlightTwoTone fontSize="small" />
                    </div>
                    <div className="days">
                        11D · 10N
                    </div>
                </div>
                <div className="price-edit">
                    <span className="price">₹ 1,20,000</span>
                    <button className="edit">
                        Edit
                    </button>
                </div>
            </div>
        );
    }
}

export default One;