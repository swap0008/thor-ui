import React, { Component } from 'react';

class Two extends Component {
    render() {
        return (
            <div className="event-name-card">
                <div className="image-layer" />
                <div className="image-center">
                    <span>Hotels In</span>
                    <span className="location">India</span>
                </div>
            </div>
        );
    }
}

export default Two;