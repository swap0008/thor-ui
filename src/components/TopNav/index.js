import React, { Component } from 'react';

//internationalization
import { injectIntl, defineMessages } from "react-intl";


const messages = defineMessages({
    navItem1: {
        id: 'topnav.dashboard',
        defaultMessage: 'Dashboard'
    },
    navItem2: {
        id: 'topnav.contacts',
        defaultMessage: 'Contacts'
    },
    navItem3: {
        id: 'topnav.campaign',
        defaultMessage: 'Campaign'
    },
    navItem4: {
        id: 'topnav.more',
        defaultMessage: 'More'
    },
});

class TopNav extends Component {
    render() {
        const { intl: { formatMessage } } = this.props;

        return (
            <div className="top-nav">
                <div className="nav-item selected">{formatMessage(messages.navItem1)}</div>
                <div className="nav-item">{formatMessage(messages.navItem2)}</div>
                <div className="nav-item">{formatMessage(messages.navItem3)}</div>
                <div className="nav-item">{formatMessage(messages.navItem4)}</div>
            </div>
        );
    }
}

export default injectIntl(TopNav);