import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Backdrop, TextField, Button, TextareaAutosize } from '@material-ui/core';
import { EditOutlined } from '@material-ui/icons';
import { handleUpdateCalendar } from '../../redux/actions/calendars';

class EditCalendar extends Component {
    state = {
        open: false,
        type: 'platform',
        summary: '',
        description: '',
        defaultType: ''
    }

    componentDidMount = () => {
        const { calendar } = this.props;

        this.setState({
            type: calendar.id.split(':-:')[1],
            id: calendar.id,
            description: calendar.description || '',
            summary: calendar.name,
            defaultType: calendar.id.split(':-:')[1]
        });
    }

    handleToggle = () => {
        this.setState(({ open }) => ({
            open: !open
        }));
    }

    setValue = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { type, summary, description, id } = this.state;

        const [calendarId, actualType] = id.split(':-:');

        const body = {
            "company_id": "LGG35eleOdiESJf4hyn0",
            "google_accounts": ["temporary@mdcpartners.be"],
            calendar: { id: calendarId, summary, description },
            type,
            moveToGoogle: type !== actualType
        }

        this.props.dispatch(handleUpdateCalendar(body));
        this.setState({ open: false });
    }

    render() {
        const { open, summary, type, description, defaultType } = this.state;

        return (
            <React.Fragment>
                <EditOutlined className='edit-calendar' onClick={this.handleToggle} />
                <Backdrop open={open} className="backdrop">
                    <form className="add-calendar-box" onSubmit={this.handleSubmit}>
                        <span className="close" onClick={this.handleToggle}>X</span>
                        <select value={type} onChange={this.setValue} name="type" disabled={defaultType === 'google'}>
                            <option value="platform">Platform Calendar</option>
                            <option value="google">Google Calendar</option>
                        </select>
                        <TextField
                            value={summary}
                            name="summary"
                            id="outlined-basic"
                            label="Calendar Name"
                            variant="outlined"
                            className="text-field"
                            onChange={this.setValue}
                        />
                        <TextareaAutosize
                            value={description}
                            name="description"
                            rowsMin={5}
                            className="text-area"
                            placeholder="Description"
                            onChange={this.setValue}
                        />
                        <Button variant="contained" color="primary" className="add-button" type="submit">
                            Update
                        </Button>
                    </form>
                </Backdrop>
            </React.Fragment>
        );
    }
}

export default connect()(EditCalendar);