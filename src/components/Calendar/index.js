import React, { Component } from 'react';
import { connect } from 'react-redux';
import Calendar from '@toast-ui/react-calendar';
import 'tui-calendar/dist/tui-calendar.css';
import LeftNav from '../LeftNav';
import RightNav from '../RightNav';
import {
    handleCreateCalendarEvent,
    handleDeleteCalendarEvent,
    handleUpdateCalendarEvent,
} from '../../redux/actions/calendars';

// If you use the default popups, use this.
import 'tui-date-picker/dist/tui-date-picker.css';
import 'tui-time-picker/dist/tui-time-picker.css';

import themeConfig from './themeConfig';
import template from './template';
import AddCalendar from './AddCalendar';


class CalendarComponent extends Component {
    state = {
        calendars: {},
        view: 'week'
    }

    deleteEvent = async ({ schedule }) => {
        const { dispatch } = this.props;

        const [calendarId, type] = schedule.calendarId.split(':-:');

        const body = {
            "company_id": "LGG35eleOdiESJf4hyn0",
            "google_accounts": ["temporary@mdcpartners.be"],
            "calendar_id": calendarId,
            "event_id": schedule.id,
            type
        }

        dispatch(handleDeleteCalendarEvent(body));
    }

    createEvent = async (schedule) => {
        const { dispatch } = this.props;

        console.log(schedule);

        const [calendarId, type] = schedule.calendarId.split(':-:');

        const body = {
            "company_id": "LGG35eleOdiESJf4hyn0",
            "google_accounts": ["temporary@mdcpartners.be"],
            "calendar_id": calendarId,
            event: schedule,
            type
        }

        dispatch(handleCreateCalendarEvent(body));
    }


    updateEvent = async ({ schedule, changes }) => {
        const [calendarId, type] = schedule.calendarId.split(':-:');

        const body = {
            "company_id": "LGG35eleOdiESJf4hyn0",
            "google_accounts": ["temporary@mdcpartners.be"],
            "calendar_id": calendarId,
            event: { ...schedule, ...changes },
            type
        }

        if (changes.calendarId) {
            body.from = calendarId;
            body.to = changes.calendarId
        }

        this.props.dispatch(handleUpdateCalendarEvent(body));
    }


    render() {
        const { calendars = {}, events = [] } = this.props;

        return (
            <div className="sections">
                <LeftNav />
                <div className="calendar">
                    <div className="calendar-options">
                        <select
                            value={this.state.view}
                            className="top-left-select"
                            onChange={(e) => this.setState({ view: e.target.value })}
                        >
                            <option value="day">Day</option>
                            <option value="week">Week</option>
                            <option value="month">Month</option>
                        </select>

                        <AddCalendar />
                    </div>
                    {events.length !== 0 && (
                        <Calendar
                            height="900px"
                            calendars={Object.keys(calendars).map(key => calendars[key])}
                            disableDblClick={true}
                            disableClick={false}
                            isReadOnly={false}
                            schedules={events}
                            view={this.state.view}
                            taskView={false}
                            scheduleView={true}
                            month={{
                                startDayOfWeek: 0
                            }}
                            template={template}
                            theme={themeConfig}
                            useDetailPopup={true}
                            useCreationPopup={true}
                            onBeforeDeleteSchedule={(e) => this.deleteEvent(e)}
                            onBeforeCreateSchedule={(e) => this.createEvent(e)}
                            onBeforeUpdateSchedule={(e) => this.updateEvent(e)}
                        />
                    )}
                </div>
                <RightNav />
            </div>
        );
    }
}

const mapStateToProps = ({ calendars, calendarEvents }) => {
    return {
        calendars,
        events: Object.keys(calendarEvents).map(id => calendarEvents[id])
    }
}

export default connect(mapStateToProps)(CalendarComponent);