import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DeleteOutline } from '@material-ui/icons';
import {
    handleFetchCalendarEvents,
    removeCalendarEventsFilter,
    handleDeleteCalendar
} from '../../redux/actions/calendars';
import EditCalendar from './EditCalendar';

class ShowCalendar extends Component {
    state = {
        selected: ['temporary@mdcpartners.be']
    }

    handleSelect = (key) => {
        const { selected } = this.state;
        const { dispatch } = this.props;
        const [calendarId, type] = key.split(':-:');

        if (selected.indexOf(key) === -1) {
            dispatch(handleFetchCalendarEvents({
                "company_id": "LGG35eleOdiESJf4hyn0",
                "google_accounts": ["temporary@mdcpartners.be"],
                calendar_ids: [calendarId],
                type
            }))
            this.setState({ selected: selected.concat(key) });
        } else {
            this.setState({ selected: selected.filter(select => key !== select) });
            dispatch(removeCalendarEventsFilter(key));
        }
    }

    removeCalendar = (key) => {
        const { dispatch } = this.props;
        const [calendar_id, type] = key.split(':-:');

        let body = {
            calendar_id,
            type
        };

        if (type === 'platform') return dispatch(handleDeleteCalendar(body));

        body = {
            ...body,
            "company_id": "LGG35eleOdiESJf4hyn0",
            "google_accounts": ["temporary@mdcpartners.be"]
        };

        dispatch(handleDeleteCalendar(body));
    }

    render() {
        const { calendars = {} } = this.props;
        const { selected } = this.state;

        return (
            <div className="calendars">
                {Object.keys(calendars).map(key => (
                    <div key={key} className="calendar-right-nav">
                        <span style={{
                            backgroundColor: selected.indexOf(key) === -1 ? 'white' : calendars[key].bgColor,
                            border: `1.5px solid ${calendars[key].bgColor}`
                        }} onClick={() => this.handleSelect(key)}></span>
                        <span>{calendars[key].name}</span>
                        <DeleteOutline className="delete-calendar" onClick={() => this.removeCalendar(key)} />
                        <EditCalendar calendar={calendars[key]} />
                    </div>
                ))}
            </div>
        );
    }
}

const mapStateToProps = ({ calendars }) => {
    return {
        calendars
    }
}

export default connect(mapStateToProps)(ShowCalendar);