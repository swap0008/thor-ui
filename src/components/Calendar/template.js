import moment from 'moment';

const template = {
    time: (schedule) => {
        const startTime = moment(schedule.start.getTime()).format('HH:mm');
        const title = schedule.title;

        return `<strong>${startTime}</strong> ${title}`;
    },
    collapseBtnTitle: function() {
        return '<span class="tui-full-calendar-icon tui-full-calendar-ic-arrow-solid-top">Swapnil</span>';
    }
}

export default template;