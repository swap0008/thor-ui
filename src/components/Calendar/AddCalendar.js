import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Backdrop, TextField, TextareaAutosize, Button } from '@material-ui/core';
import { handleAddCalendar } from '../../redux/actions/calendars';

class AddCalendar extends Component {
    state = {
        open: false,
        type: 'platform',
        summary: '',
        description: ''
    }

    handleToggle = () => {
        this.setState(({ open }) => ({
            open: !open
        }));
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { type, summary, description } = this.state;

        const body = {
            calendar: { summary, description },
            type
        };

        if (type === 'platform') {
            this.props.dispatch(handleAddCalendar(body))
        } else {
            this.props.dispatch(handleAddCalendar({
                "company_id": "LGG35eleOdiESJf4hyn0",
                "google_accounts": ["temporary@mdcpartners.be"],
                ...body
            }))
        }

        this.setState({ open: false, summary: '', description: '' });
    }

    setValue = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        const { open, summary, type, description } = this.state;

        return (
            <React.Fragment>
                <div className="add-calendar" onClick={this.handleToggle}>
                    Add Calendar
                </div>
                <Backdrop open={open} className="backdrop">
                    <form className="add-calendar-box" onSubmit={this.handleSubmit}>
                        <span className="close" onClick={this.handleToggle}>X</span>
                        <select value={type} onChange={this.setValue} name="type">
                            <option value="platform">Platform Calendar</option>
                            <option value="google">Google Calendar</option>
                        </select>
                        <TextField
                            value={summary}
                            name="summary"
                            id="outlined-basic"
                            label="Calendar Name"
                            variant="outlined"
                            className="text-field"
                            onChange={this.setValue}
                        />
                        <TextareaAutosize
                            value={description}
                            name="description"
                            rowsMin={5}
                            className="text-area"
                            placeholder="Description"
                            onChange={this.setValue}
                        />
                        <Button variant="contained" color="primary" className="add-button" type="submit">
                            Add
                        </Button>
                    </form>
                </Backdrop>
            </React.Fragment>
        );
    }
}

export default connect()(AddCalendar);