import React, { Component } from 'react';
import { Fab } from '@material-ui/core';
import { Search, Add } from '@material-ui/icons';
import TripTable from './TripTable';
import LeftNav from '../LeftNav';
import RightNav from '../RightNav';

class Proposal extends Component {
	render() {
		return (
			<div className="sections">
				<LeftNav />
				<div className="proposal">
					<div className="search">
						<input type="text" className="search-box" placeholder="Search" />
						<Search className="search-icon" />
					</div>
					<div className="filter" />
					<TripTable />

					<div style={{ textAlign: 'center' }}>
						<Fab color="primary" aria-label="add" size="small" style={{ marginTop: 20 }}>
							<Add />
						</Fab>
					</div>
				</div>
				<RightNav />
			</div>
		);
	}
}

export default Proposal;
