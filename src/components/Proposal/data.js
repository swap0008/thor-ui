import { defineMessages } from 'react-intl';

export const tripTable = defineMessages([
	{
		id: 'tableheader',
		values: [
			{
				id: 'triptable.proposal',
				defaultMessage: 'Proposal'
			},
			{
				id: 'triptable.client',
				defaultMessage: 'Client'
			},
			{
				id: 'triptable.duration',
				defaultMessage: 'Duration'
			},
			{
				id: 'triptable.cost',
				defaultMessage: 'Cost'
			},
			{
				id: 'triptable.dates',
				defaultMessage: 'Dates'
			},
			{
				id: 'triptable.status',
				defaultMessage: 'Status'
			}
		]
	}
]);
