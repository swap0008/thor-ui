import React from 'react';
import { connect } from 'react-redux';

const TripTableDetails = (props) => {
    const { proposal = {} } = props;

    return (
        <div>
            {Object.keys(proposal).map(key => <p key={key}>{key} : {proposal[key]}</p>)}
        </div>
    );
}

const mapStateToProps = ({ proposals }, props) => {
    const { tripId } = props.match.params;

    return {
        proposal: proposals[tripId]
    };
}

export default connect(mapStateToProps)(TripTableDetails);