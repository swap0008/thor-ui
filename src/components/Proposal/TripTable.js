import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import {
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	Paper,
	TableContainer,
	makeStyles
} from '@material-ui/core';
import { tripTable as messages } from './data';

const useStyles = makeStyles((theme) => ({
	wrapper: {
		marginTop: theme.spacing(4)
	},
	header: {
		backgroundColor: 'rgba(193,212,241,0.5)'
	},
	head: {
		color: 'rgb(43, 89, 162)',
		fontSize: '16.3px',
		fontStyle: 'italic',
		fontFamily: `'Lato', Arial`,
		fontWeight: 600
	},
	subHead: {
		fontSize: '16.3px',
		fontStyle: 'italic',
		fontFamily: `'Lato', Arial`,
		cursor: 'pointer'
	}
}));

const parseDates = (dates) => {
	let [ from, to ] = dates.split('-');
	from = new Date(parseInt(from)).toDateString().split(' ');
	to = new Date(parseInt(to)).toDateString().split(' ');

	return `${from[1]} ${from[2]} - ${to[1]} ${to[2]}`;
}

const TripTable = (props) => {
	const classes = useStyles();
	const { history: { push }, match: { params: { lang } }, intl: { formatMessage }, proposals = {} } = props;

	return (
		<TableContainer component={Paper} className={classes.wrapper}>
			<Table aria-label="simple table">
				<TableHead>
					<TableRow className={classes.header}>
						{messages[0].values.map((value) => (
							<TableCell className={classes.head} key={value.id}>
								{formatMessage(value)}
							</TableCell>
						))}
					</TableRow>
				</TableHead>
				{Object.keys(proposals).map(id => {
					const { name, client, duration, cost, dates, status } = proposals[id];
					return (
						<TableBody key={id}>
							<TableRow hover role="checkbox" onClick={() => push(`/${lang}/proposals/${id}`)}>
								<TableCell className={classes.subHead}>{name}</TableCell>
								<TableCell className={classes.subHead}>{client}</TableCell>
								<TableCell className={classes.subHead}>{duration}</TableCell>
								<TableCell className={classes.subHead}>${cost}</TableCell>
								<TableCell className={classes.subHead}>{parseDates(dates)}</TableCell>
								<TableCell className={classes.subHead}>{status}</TableCell>
							</TableRow>
						</TableBody>
					)
				})}
			</Table>
		</TableContainer>
	);
};

const mapStateToProps = ({ proposals }) => {
	return {
		proposals
	};
}

export default injectIntl(withRouter(connect(mapStateToProps)(TripTable)));
