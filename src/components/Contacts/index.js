import React, { Component } from 'react';
import { Contacts, AddContact } from 'contacts-component';

const VIEW_CONTACTS = 'VIEW_CONTACTS';
const ADD_CONTACTS = 'ADD_CONTACTS';
const EDIT_CONTACTS = 'EDIT_CONTACTS';

class ContactsComponent extends Component {
    state = {
        view: VIEW_CONTACTS
    }

    changeView = () => {

    }

    render() {
        const { view } = this.state;

        switch (view) {
            case VIEW_CONTACTS:
                return <Contacts />
            case ADD_CONTACTS:
                return <AddContact />
            default:
                return <Contacts />
        }
    }
}