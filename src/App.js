import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

//Components
import Layout from './components/Layout';
import ChangeLanguage from './ChangeLanguage';

//internationalization
import { IntlProvider } from 'react-intl';
import translations from './i18n/locales';

class App extends Component {
	state = {
		lang: null,
		value: 'en'
	};

	componentDidMount() {
		const webLang = window.location.href.split('/')[3];

		this.setState({
			value: translations[webLang] ? webLang : 'en'
		});
	}

	handleClick = (event) => {
		this.setState({ lang: event.currentTarget });
	};

	handleClose = (e) => {
		const webLang = e.target.textContent;
		this.setState({
			lang: null,
			value: webLang ? webLang : this.state.value
		});
	};

	render() {
		const { lang, value } = this.state;
		const messages = translations[value];

		return (
			<IntlProvider locale="en" messages={messages}>
				<Router>
					{/* <ChangeLanguage
						handleClick={this.handleClick}
						handleClose={this.handleClose}
						lang={lang}
						value={value}
					/> */}
					<Route exact path="/">
						<Redirect to="/en" />
					</Route>
					<Route path="/:lang" component={Layout} />
				</Router>
			</IntlProvider>
		);
	}
}

export default App;
