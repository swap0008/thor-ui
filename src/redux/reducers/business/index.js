import {
    FETCH_BUSINESS,
    SAVE_BUSINESS
} from '../../actions/actionTypes';

export default function (state = {}, action) {
    const { business } = action;

    switch (action.type) {
        case FETCH_BUSINESS:
            return state;
        case SAVE_BUSINESS:
            return {
                ...state,
                [business.id]: business
            }
        default:
            return state;
    }
}

