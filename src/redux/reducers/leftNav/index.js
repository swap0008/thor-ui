import { FETCH_LEFT_NAV_ITEMS } from '../../actions/actionTypes';

export default function (state = [], action) {
    switch (action.type) {
        case FETCH_LEFT_NAV_ITEMS:
            return [
                ...action.leftNavItems
            ]
        default:
            return state;
    }
}