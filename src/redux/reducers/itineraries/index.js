import { FETCH_ITINERARIES, SAVE_ITINERARY } from '../../actions/actionTypes';

export default function itineraries(state = {}, action) {
    switch(action.type) {
        case FETCH_ITINERARIES:
            return {
                ...state,
                ...action.itineraries
            }
        case SAVE_ITINERARY:
            break;
            // const { itinerary } = action;
            // return {
            //     ...state,
            //     []
            // }
        default: 
            return state;
    }
}