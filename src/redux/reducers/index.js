import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading';
import itineraries from './itineraries';
import proposals from './proposals';
import events from './events';
import calendars from './calendars';
import calendarEvents from './calendars/events';
import leftNavItems from './leftNav';
import business from './business';
import { businessReducer } from 'business-component';
import { contactsReducer } from 'contacts-component'
import itinerariesBasic from '../../components/Itinerary-Basic/reducers';
import me from './auth';

export default combineReducers({
    itineraries,
    proposals,
    events,
    calendars,
    calendarEvents,
    loadingBar: loadingBarReducer,
    leftNavItems,
    contacts: contactsReducer,
    business,
    businesses: businessReducer,
    itinerariesBasic,
    me
});