import {
    FETCH_CALENDARS,
    ADD_CALENDAR,
    DELETE_CALENDAR,
    UPDATE_CALENDAR
} from '../../actions/actionTypes';

export default function calendars(state = {}, action) {
    const { calendars, calendar } = action;

    switch (action.type) {
        case FETCH_CALENDARS:
            return {
                ...state,
                ...calendars
            };
        case ADD_CALENDAR:
            return {
                ...state,
                [calendar.id]: calendar
            }
        case DELETE_CALENDAR:
            delete state[calendar.id];
            return { ...state };
        case UPDATE_CALENDAR:
            if (action.moveToGoogle) {
                delete state[action.calendarId];
                console.log('Calendar Reducer', state);
                return {
                    ...state,
                    [calendar.calendar.id]: calendar.calendar
                }
            }

            state[calendar.id] = calendar;
            return { ...state };
        default:
            return state;
    }
}