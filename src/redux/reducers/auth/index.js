import {
	LOGIN,
	LOGGED,
	SIGN_UP
} from '../../actions/actionTypes';

export default function (state = {}, action) {
	const { isFetching } = action;

	switch(action.type) {
		case LOGIN:
			return {
				...state,
				isFetching
			}
		case LOGGED:
			return {
				...state,
				data: action.me,
				isFetching
			}
		case SIGN_UP:
			return {
				...state,
				isFetching
			}
		default:
			return state;
	}
}