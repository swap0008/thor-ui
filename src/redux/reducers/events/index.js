import {
    FETCH_EVENTS,
    SAVE_EVENT,
    UPDATE_ROW,
    DELETE_EVENT
} from '../../actions/actionTypes';

export default function events(state = [], action) {
    const { event, day, event: { row, column } = {}, day: { row: { position, number } = {}, type } = {} } = action;

    switch (action.type) {
        case FETCH_EVENTS:
            return action.events;
        case SAVE_EVENT:
            delete event.row;
            delete event.column;
            state[row].columns[column].events.splice(event.position, 0, event);
            return [...state];
        case UPDATE_ROW:
            day.row['events'] = [];
            if (type === 'delete') {
                state.splice(position, 1);
            } else if (type === 'add') {
                const newRow = {};
                newRow.columns = [];
                for (let i = 0; i < day.row.columns; i++) {
                    newRow.columns.push({ events: [] });
                }
                state.splice(position, 0, newRow);
            } else if (type === 'move_up') {
                if (state.length === 1) return state;
                const temp = state[position - 1];
                state[position - 1] = state[position];
                state[position] = temp;
            } else if (type === 'move_down') {
                if (state.length === 1) return state;
                const temp = state[position + 1];
                state[position + 1] = state[position];
                state[position] = temp;
            } else if (type === 'move_event_up') {
                if (state[number].columns[day.row.column].events === 1) return state;
                const temp = state[number].columns[day.row.column].events[position - 1];
                state[number].columns[day.row.column].events[position - 1] = state[number].columns[day.row.column].events[position];
                state[number].columns[day.row.column].events[position] = temp;
            } else if (type === 'move_event_down') {
                if (state[number].columns[day.row.column].events === 1) return state;
                const temp = state[number].columns[day.row.column].events[position + 1];
                state[number].columns[day.row.column].events[position + 1] = state[number].columns[day.row.column].events[position];
                state[number].columns[day.row.column].events[position] = temp;
            }
            return [...state];
        case DELETE_EVENT:
            state[row].columns[column].events.splice(event.position, 1);
            return [...state];
        default:
            return state;
    }
}