import { FETCH_PROPOSALS } from '../../actions/actionTypes';

export default function itineraries(state = {}, action) {
    switch(action.type) {
        case FETCH_PROPOSALS:
            return {
                ...state,
                ...action.proposals
            }
        default: 
            return state;
    }
}