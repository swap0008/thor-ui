import { fetchItineraries } from './itineraries';
import { fetchProposals } from './proposals';
import { showLoading, hideLoading } from 'react-redux-loading';
import { fetchCalendars, handleFetchCalendarEvents } from './calendars';
import axiosInstance from '../../axios';

const axios = axiosInstance('');
const body = {
    "company_id": "LGG35eleOdiESJf4hyn0",
    "google_accounts": ["temporary@mdcpartners.be"]
};

export const handleInitialData = () => {
    return async (dispatch) => {
        try {
            dispatch(showLoading())
            const result = await Promise.all([
                axios.get('itinerary/all'),
                axios.get('proposal/all'),
                axios.post('calendar/list', body)
            ]);
            dispatch(fetchItineraries(result[0].data));
            dispatch(fetchProposals(result[1].data));
            dispatch(fetchCalendars(result[2].data));
            // dispatch(handleFetchCalendarEvents({
            //     ...body,
            //     calendar_ids: Object.keys(result[2].data).slice(0, 1)
            // }))
            dispatch(hideLoading());
        } catch (e) {
            console.log(e);
        }
    }
}