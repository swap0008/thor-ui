import {
    FETCH_ITINERARIES,
    SAVE_ITINERARY,
    DELETE_ITINERARY,
    UPDATE_ITINERARY
} from '../actionTypes';
import axiosInstance from '../../../axios';

const axios = axiosInstance('itinerary');

export const fetchItineraries = (itineraries) => {
    return {
        type: FETCH_ITINERARIES,
        itineraries
    };
}

export const saveItinerary = (itinerary) => {
    return {
        type: SAVE_ITINERARY,
        itinerary
    };
}

export const deleteItinerary = (itinerary) => {
    return {
        type: DELETE_ITINERARY,
        itinerary
    };
}

export const updateItinerary = (itinerary) => {
    return {
        type: UPDATE_ITINERARY,
        itinerary
    }
}

export const handleSaveItinerary = (itinerary) => {
    itinerary = {
        company_id: 'bindass@travel.com',
        employee_id: 'munna@travel.com',
        ...itinerary
    };
    return async (dispatch) => {
        try {
            const { data } = await axios.post('/', {
                company_id: 'bindass@travel.com',
                employee_id: 'munna@travel.com',
                ...itinerary
            });
            console.log(data);
            // dispatch(saveItinerary(data));
        } catch (e) {
            console.log(e);
        }
    }
}