import {
    FETCH_DAYS,
    UPDATE_ROW
} from '../actionTypes';
import axiosInstance from '../../../axios';

const axios = axiosInstance('day');

export const fetchDays = (days) => {
    return {
        type: FETCH_DAYS,
        days
    }
}

export const updateRow = (day) => {
    return {
        type: UPDATE_ROW,
        day
    }
}

export const handleUpdateRow = (day) => {
    return async (dispatch) => {
        await axios.put('/', day);
        dispatch(updateRow(day));
    }
}
