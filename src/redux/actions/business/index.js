import {
    FETCH_BUSINESS,
    SAVE_BUSINESS
} from '../actionTypes';
import Axios from 'axios';

const fetchBusiness = (business) => {
    return {
        type: FETCH_BUSINESS,
        business
    };
}

export const handleFetchBusiness = () => {
    return async (dispatch) => {

    }
}

const saveBusiness = (business) => {
    return {
        type: SAVE_BUSINESS,
        business
    }
}

export const handleSaveBusiness = (business_details) => {
    return async (dispatch) => {
        try {
            const { data } = await Axios({
                method: 'post',
                url: 'http://localhost:4000/',
                data: { business_details }
            });

            const { business, token } = data;
            localStorage.setItem('token', token);

            dispatch(saveBusiness(business));
        } catch (err) {
            console.log(err);
        }

    }
}