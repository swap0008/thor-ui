export const CAN_ACCESS_GOOGLE_CONTACTS = 'CAN_ACCESS_GOOGLE_CONTACTS';
export const CAN_ACCESS_PLATFORM_CONTACTS = 'CAN_ACCESS_PLATFORM_CONTACTS';
export const CAN_ACCESS_CONTACTS = 'CAN_ACCESS_CONTACTS';

const leftNavItems = [
    {
        name: 'Analytics',
        items: [
            { name: 'Dashboard', link: 'dashboard', permissions: [] },
            { name: 'Customers', link: 'customers', permissions: [] },
            { name: 'Reports', link: 'reports', permissions: [] }
        ],
        link: 'analytics',
        logo: 'ti-bar-chart'
    },
    {
        name: 'Applications',
        items: [
            {
                name: 'Contacts',
                link: 'contacts',
                permissions: [[CAN_ACCESS_GOOGLE_CONTACTS, CAN_ACCESS_PLATFORM_CONTACTS], [CAN_ACCESS_CONTACTS]]
            },
            { name: 'Calendar', link: 'calendar', permissions: [] },
            { name: 'Itinerary', link: 'itinerary', permissions: [] }
        ],
        link: 'applications',
        logo: 'ti-folder'
    }
]


export const getLeftNavItems = (permissions) => {
    return leftNavItems.map(menu => {
        menu.items = menu.items.filter(item => {
            if (!item.permissions.length) return true;

            let and = true;
            let or = false;
            for (const permission of item.permissions) {
                for (const p of permission) {
                    or |= permissions[p];
                }
                and &= or;
                or = false;
            }

            return and;
        });

        return menu;
    });
}