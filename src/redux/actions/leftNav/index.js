import { FETCH_LEFT_NAV_ITEMS } from '../actionTypes';
import {
    getLeftNavItems,
    CAN_ACCESS_CONTACTS,
    CAN_ACCESS_PLATFORM_CONTACTS,
    CAN_ACCESS_GOOGLE_CONTACTS
} from './leftNavData';

const fetchLeftNavItems = (leftNavItems) => {
    return {
        type: FETCH_LEFT_NAV_ITEMS,
        leftNavItems
    };
}

export const handleFetchLeftNavItems = () => {
    return (dispatch) => {
        const permissions = {
            [CAN_ACCESS_CONTACTS]: true,
            [CAN_ACCESS_GOOGLE_CONTACTS]: true,
            [CAN_ACCESS_PLATFORM_CONTACTS]: true
        }

        const items = getLeftNavItems(permissions);
        dispatch(fetchLeftNavItems(items));
    }
}