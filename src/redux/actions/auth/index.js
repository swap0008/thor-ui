import Axios from 'axios';
import {
    LOGIN,
    LOGGED,
    SIGN_UP
} from '../actionTypes';

export const axiosInstance = Axios.create({
    baseURL: 'https://59phci4y38.execute-api.ap-south-1.amazonaws.com/dev'
});

const login = () => {
    return {
        type: LOGIN,
        isFetching: true
    }
}

const logged = (me) => {
    return {
        type: LOGGED,
        me,
        isFetching: false
    }
}

export const handleLogin = (loginData, { match }) => {
    return async (dispatch) => {
        try {
            dispatch(login());
            const { data } = await axiosInstance.post('/login', loginData);
            localStorage.setItem('token', data.token);
            dispatch(logged());
            window.location.replace(`/${match.params.lang}/analytics/dashboard`);
        } catch (err) {
            dispatch(logged());
            console.log(err);
        }
    }
}

const signUp = () => {
    return {
        type: SIGN_UP,
        isFetching: false
    }
}

export const handleSignUp = (signUpData, { history, match }) => {
    return async (dispatch) => {
        try {
            dispatch(login())
            await axiosInstance.post('/signup', signUpData);
            dispatch(signUp());
            history.push(`/${match.params.lang}/login`);
        } catch (err) {
            console.log(err);
        }
    }
}