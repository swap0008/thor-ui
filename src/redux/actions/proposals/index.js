import {
    FETCH_PROPOSALS,
    SAVE_PROPOSAL,
    DELETE_PROPOSAL,
    UPDATE_PROPOSAL
} from '../actionTypes';


export const fetchProposals = (proposals) => {
    return {
        type: FETCH_PROPOSALS,
        proposals
    };
}

export const saveProposal = (proposal) => {
    return {
        type: SAVE_PROPOSAL,
        proposal
    };
}

export const deleteProposal = (proposal) => {
    return {
        type: DELETE_PROPOSAL,
        proposal
    };
}

export const updateProposal = (proposal) => {
    return {
        type: UPDATE_PROPOSAL,
        proposal
    }
}

