import {
    FETCH_EVENTS,
    SAVE_EVENT,
    DELETE_EVENT
} from '../actionTypes';
import axiosInstance from '../../../axios';

const axios = axiosInstance('event');

export const fetchEvents = (events) => {
    return {
        type: FETCH_EVENTS,
        events
    }
}

export const handleFetchEvents = (dayId) => {
    return async (dispatch) => {
        try {
            const { data } = await axios.post('/all', { dayId });
            dispatch(fetchEvents(data));
        } catch (e) {
            console.log(e);
        }
    }
}

export const saveEvent = (event) => {
    return {
        type: SAVE_EVENT,
        event
    }
}

export const handleSaveEvent = (event) => {
    return async (dispatch) => {
        const { data } = await axios.post('/save', event);
        dispatch(saveEvent({
            ...event,
            id: data
        }));
    }
}

export const deleteEvent = (event) => {
    return {
        type: DELETE_EVENT,
        event
    }
}

export const handleDeleteEvent = (event) => {
    return async (dispatch) => {
        await axios.delete('/', { data: event });
        dispatch(deleteEvent(event));
    }
}

