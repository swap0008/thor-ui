export const mapToTuiEvents = (events, calendarId) => {
    return events.map(event => ({
        id: event.id,
        calendarId: calendarId,
        start: event.start.dateTime,
        end: event.end.dateTime,
        title: event.summary,
        location: event.location || '',
        category: 'time'
    }));
}

export const mapToTuiCalendars = (calendars) => {
    return calendars.map(calendar => ({
        id: calendar.id,
        name: calendar.summary,
        bgColor: calendar.backgroundColor,
        color: calendar.foregroundColor,
        events: []
    }));
}

export const mapToGoogleEvent = (schedule) => {
    return {
        summary: schedule.title,
        location: schedule.location,
        end: {
            dateTime: new Date(schedule.end._date).toISOString()
        },
        start: {
            dateTime: new Date(schedule.start._date).toISOString()
        }
    };
}

export const mapScheduleToTuiEvent = (schedule) => {
    return {
        calendarId: schedule.calendarId,
        title: schedule.title,
        end: new Date(schedule.end._date).toISOString(),
        start: new Date(schedule.start._date).toISOString(),
        location: schedule.location
    }
}