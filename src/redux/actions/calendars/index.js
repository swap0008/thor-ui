import {
    FETCH_CALENDARS,
    CREATE_CALENDAR_EVENT,
    FETCH_CALENDAR_EVENTS,
    DELETE_CALENDAR_EVENT,
    UPDATE_CALENDAR_EVENT,
    REMOVE_CALENDAR_EVENTS_FILTER,
    ADD_CALENDAR,
    DELETE_CALENDAR,
    UPDATE_CALENDAR
} from '../actionTypes';
import {
    mapToTuiCalendars
} from './helpers';
import { showLoading, hideLoading } from 'react-redux-loading';
import axiosInstance from '../../../axios';

const axios = axiosInstance('calendar');

export const fetchCalendars = (calendars) => {
    return {
        type: FETCH_CALENDARS,
        calendars
    }
}

export const handleFetchCalendars = (body) => {
    return async (dispatch) => {
        dispatch(showLoading());

        const { data } = await axios.post('/list', body);
        dispatch(fetchCalendars(mapToTuiCalendars(data)));

        dispatch(handleFetchCalendarEvents({
            ...body,
            calendar_ids: data.map(calendar => calendar.id)
        }));

        dispatch(hideLoading());
    }
}

const fetchCalendarEvents = (events) => {
    return {
        type: FETCH_CALENDAR_EVENTS,
        events
    }
}

export const handleFetchCalendarEvents = (body) => {
    return async (dispatch) => {
        const { data } = await axios.post('/events', body);
        dispatch(fetchCalendarEvents(data));
    }
}

const createCalendarEvent = (event) => {
    return {
        type: CREATE_CALENDAR_EVENT,
        event
    }
}

export const handleCreateCalendarEvent = (body) => {
    return async (dispatch) => {
        const { data } = await axios.post('/event', body);
        data.calendarId = `${data.calendarId}:-:${body.type}`;
        dispatch(createCalendarEvent(data));
    }
}

const deleteCalendarEvent = (event) => {
    return {
        type: DELETE_CALENDAR_EVENT,
        event
    }
}

export const handleDeleteCalendarEvent = (body) => {
    return async (dispatch) => {
        await axios.delete('/event', {
            data: body
        });
        dispatch(deleteCalendarEvent({ id: body.event_id }));
    }
}

const updateCalendarEvent = (event) => {
    return {
        type: UPDATE_CALENDAR_EVENT,
        event
    }
}

export const handleUpdateCalendarEvent = (body) => {
    return async (dispatch) => {
        const event = await axios.put('/event', body);
        dispatch(updateCalendarEvent(event.data));
    }
}

export const removeCalendarEventsFilter = (calendarId) => {
    return {
        type: REMOVE_CALENDAR_EVENTS_FILTER,
        calendarId
    }
}

const addCalendar = (calendar) => {
    return {
        type: ADD_CALENDAR,
        calendar
    }
}

export const handleAddCalendar = (calendar) => {
    return async (dispatch) => {
        const { data } = await axios.post('/', calendar);
        dispatch(addCalendar(data));
    }
}

const deleteCalendar = (calendar) => {
    return {
        type: DELETE_CALENDAR,
        calendar
    }
}

export const handleDeleteCalendar = (body) => {
    return async (dispatch) => {
        await axios.delete('/', { data: body })
        dispatch(deleteCalendar({ id: `${body.calendar_id}:-:${body.type}` }));
    }
}

const updateCalendar = (calendar, moveToGoogle, calendarId) => {
    return {
        type: UPDATE_CALENDAR,
        calendar,
        moveToGoogle,
        calendarId
    }
}

export const handleUpdateCalendar = (body) => {
    return async (dispatch) => {
        const { data } = await axios.put('/', body);
        dispatch(updateCalendar(data, body.moveToGoogle, `${body.calendar.id}:-:platform`));
    }
}