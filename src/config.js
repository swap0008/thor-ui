import axios from 'axios';

const services = ['contacts', 'business', 'itinerary_basic']

const config = {
	local: {
		'contacts': 'http://localhost:4004'
	},
	development: {
		'contacts': 'https://uojzd8irf5.execute-api.ap-south-1.amazonaws.com/dev',
		'business': 'https://wgow96ct36.execute-api.ap-south-1.amazonaws.com/dev',
		'itinerary_basic': 'https://rt29mqfxv0.execute-api.ap-south-1.amazonaws.com/dev'
	},
	prod: {}
}

const env = process.env.NODE_ENV || 'local';

const d = config[env];

console.log('------------', d, env);

const axiosInstances = {}

for (const service of services) {
	axiosInstances[service] = axios.create({
		baseURL: d[service]
	})
}

window.axiosInstances = axiosInstances;
export {axiosInstances};