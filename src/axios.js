import axios from 'axios';

const getInstance = (path) => {
	return axios.create({
		baseURL: `http://localhost:4000/${path}`
	})
}

export default getInstance;
